#!/usr/bin/python3
from setuptools import setup, find_packages

datas = [('/usr/share/applications', ['data/karpuz.desktop']),
         ('/usr/share/gtksourceview-3.0/styles', ['styles/monokai.xml']),
         ('/usr/share/gtksourceview-4/styles', ['styles/monokai.xml']),
         ('/usr/share/icons/hicolor/scalable/apps', ['icons/karpuz.svg'])
         ]


setup(
    name = "karpuz",
    scripts = ["karpuz.py"],
    packages = find_packages(),
    version = "1.0",
    description = "SoNAkıncı",
    author = ["Fatih Kaya"],
    author_email = "sonakinci41@gmail.com",
    url = "https://gitlab.com/sonakinci41/karpuz",
    data_files = datas
)
