#!/usr/bin/python3

import gi, os
from gi.repository import Gtk, GtkSource, Gdk
from sample import kelime_tamamlama

class TextEditorTab(Gtk.HBox):
	def __init__(self,ebeveyn,baslik_text,editor):
		Gtk.HBox.__init__(self)
		self.ebeveyn = ebeveyn
		#Yazının değişip değişmediğini tabta tutuyoruz çünkü tab kapatma yeri
		self.degisti_mi = False
		#Başlık için bir label oluşturuyoruz
		self.baslik_label = Gtk.Label()
		#Başlığımıza gönderilen text ekleyelim
		self.tab_baslik_ata(baslik_text)
		#Başlığımızı taşıyıcıya ekliyoruz
		self.pack_start(self.baslik_label, expand = False, fill = False, padding = 0)
		#Kapat butonu için resim oluşturuyoruz
		kapat_resim = Gtk.Image()
		#Butona stock iconlardan close iconunu ekliyoruz
		kapat_resim.set_from_stock(Gtk.STOCK_CLOSE, Gtk.IconSize.MENU)
		#Kapat dugmesi oluşturuyoruz
		kapat_dugme = Gtk.Button()
		kapat_dugme.props.relief = Gtk.ReliefStyle.NONE
		#Kapat dugmeye tıklanınca editor_tab_kapat fonksiyonu çalışacak
		kapat_dugme.connect("clicked",self.editor_tab_kapat,editor)
		#Kapat dugmeye resimi veriyoruz
		kapat_dugme.set_image(kapat_resim)
		#Duğmeyi taşıyıcıya veriyoruz
		self.pack_end(kapat_dugme, expand = False, fill = False, padding = 5)
		#Taşıyıcıyı görünür yapıyoruz
		self.show_all()

	def tab_baslik_ata(self,baslik_text):
		"""Taba başlık eklemek amaçlı yazıldı"""
		#Uzun dosya adlarını yazamayacağımız için kontrol etmeliyiz
		#Yazılan textin uzunluğuna bakalım
		sayi = len(baslik_text)
		#20 den büyükse son 20 karakteri alalım
		if  sayi > 20:
			self.baslik_label.set_text("."+baslik_text[sayi-19:])
		#20 ise sıkıntı yok
		elif sayi == 20:
			self.baslik_label.set_text(baslik_text)
		#Eğer 20 den azsa tab genişliği farklı durmaması için boşlukla 20 ye tamamlıyoruz
		else:
			self.baslik_label.set_text(baslik_text.ljust(20," "))


	def editor_tab_kapat(self,widget,editor):
		"""Bir Tab Kapatılınca Bu Fonksiyon Çalışıyor Ve Tabın Kapatılıp Kapatılamayacağına Karar Veriyoruz"""
		#Tabın numarasını alıyoruz
		acikmi, secilen = self.ebeveyn.dosya_acikmi_kontrol(editor.acilan_dosya)
		#FIX ME Burada dosya değişmiş mi bir kontrol sunmalıyız laps diye kapatmak olmaz değil mi?
		buf = editor.editor_buffer
		#dosyanın konumunu alıyoruz
		yol = editor.acilan_dosya
		text = buf.get_text(buf.get_start_iter(), buf.get_end_iter(),True)
		d_text = self.ebeveyn.dosya_oku(yol)
		if text != d_text:
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.NONE, "Kaydedilmemiş Dosyalar Var")
			dialog.format_secondary_text("Belge değiştirildi ancak kaydedilmedi. Ne yapmak istersiniz?")
			dialog.add_buttons("Kaydet Ve Kapat", Gtk.ResponseType.APPLY)
			dialog.add_buttons("Vazgeç", Gtk.ResponseType.CANCEL)
			dialog.add_buttons("Kapat", Gtk.ResponseType.CLOSE)
			donen = dialog.run()
			#Kullanıcı kadet demişse kaydedelim
			if donen == Gtk.ResponseType.APPLY:
				self.ebeveyn.d_kaydet_basildi(None)
				self.ebeveyn.editor_tablar.remove_page(secilen)
				dialog.destroy()
			#Kullanıcı vazgeçtiyse geri dönelim birşey yapmayalım
			elif donen == Gtk.ResponseType.CANCEL:
				dialog.destroy()
			#Kullanıcı kapat dediyse kapatalım
			elif donen == Gtk.ResponseType.CLOSE:
				self.ebeveyn.editor_tablar.remove_page(secilen)
				dialog.destroy()
		else:
			self.ebeveyn.editor_tablar.remove_page(secilen)



class TextEditor(Gtk.ScrolledWindow):
	def __init__(self,ebeveyn,acilan_dosya):
		Gtk.ScrolledWindow.__init__(self)
		self.ebeveyn = ebeveyn
		#Tabda açılan dosyayı saklıyoruz kayıt vs. işleri için
		self.acilan_dosya = acilan_dosya
		#Dosya yöneticisi konumunu atayacağız ama yeni belgeyse neyi atayacağız?
		#gelen arkadaşı bölelim
		bol_acilan = self.acilan_dosya.split()
		#Yeni belgemi kontrol edelim
		if bol_acilan[0] == "Yeni" and bol_acilan[1] == "Belge":
			#Yeni belge ise o zaman kullanıcı ana dizinini veriyoruz
			self.dosya_y_konum = os.path.expanduser('~')
		else:
			#Değilse tamamdır orası olsun
			self.dosya_y_konum = os.path.split(acilan_dosya)[0]
		#Dosyada değişiklik yapıldı mı
		self.dosya_degistimi = False
		#Scrool için yatay ve dikey aktif ediyoruz
		self.set_hexpand(True)
		self.set_vexpand(True)
		#Text Editörümüzü oluşturuyoruz
		self.editor = GtkSource.View()
		#Editörü Scrool Windowa ekliyoruz
		self.add(self.editor)
		#Editörün ayarları için fonksiyonumuzu çalıştırıyoruz
		self.editor_varsayilan_ayarlar()
		#Buffer yazı işleri için gerekli oluşturuyoruz
		self.editor_buffer = GtkSource.Buffer()
		#Bir tuşa basıldığında changed sinyali ve yazi_degisti çalışır
		#self.editor_buffer.connect("changed",self.yazi_degisti)
		#Editöre Buffer ekliyoruz
		self.editor.set_buffer(self.editor_buffer)
		#Buffere şema ekliyoruz
		self.renk_semasi_ata(self.ebeveyn.ayarlar["Renk_sema"])
		#Editör için bir dil aracı oluşturuyoruz
		lm = GtkSource.LanguageManager()
		#Karpuz Python editör olduğu için Pythonu ekliyoruz
		self.editor_buffer.set_language(lm.get_language('python'))

		"""OTOMATİK TAMAMLAMA"""
		#Kelime Tamamlama Sınıfını oluşuturyoruz ve kelime_tamamlama_yeni ye atıyoruz
		gsv_tamamlama = self.editor.get_completion()
		#Otomatik kelime tamamlama işlemi oluşturuyoruz
		gsv_otomatik_kelime_tamamlama = GtkSource.CompletionWords.new('main')
		#Otomatik tamamlamaya bizim bufferi veriyoruz
		gsv_otomatik_kelime_tamamlama.register(self.editor_buffer)
		#Providere ekliyoruz
		gsv_tamamlama.add_provider(gsv_otomatik_kelime_tamamlama)

		#Yeni buffer oluşturuyoruz
		keybuff = GtkSource.Buffer()
		#Aksiyon ayarı yapıyoruz
		keybuff.begin_not_undoable_action()
		keybuff.end_not_undoable_action()
		#Kelime tamamlama oluşturuyoruz
		gsv_kelime_tamamlama = GtkSource.CompletionWords.new('keyword')
		#Keybufu veriyoruz
		gsv_kelime_tamamlama.register(keybuff)
		#Tamamlamaya gönderiyoruz
		gsv_tamamlama.add_provider(gsv_kelime_tamamlama)

		#Sınıfı oluşturuyoruz
		self.kelime_tamamlama_yeni = kelime_tamamlama.KelimeTamamlama()
		#Bizim tamamlayıcıyı ekliyoruz
		gsv_tamamlama.add_provider(self.kelime_tamamlama_yeni)

	def acilan_dosya_degis(self,yol):
		#Tabda açılan dosyayı saklıyoruz kayıt vs. işleri için
		self.acilan_dosya = yol
		#Dosya yöneticisi konumu
		self.dosya_y_konum = yol


	def font_ekle(self,font):
		"""Fontu ekliyoruz"""
		self.editor.modify_font(font)		

	def belgeye_yazi_ekle(self,yazi):
		"""Belgeye yazı eklemek için bu fonksiyonu yazıyoruz"""
		self.editor_buffer.set_text(yazi)
		yazi = yazi.split("\n")
		for satir_kelimeler in yazi:
			if "import " in satir_kelimeler:
				self.kelime_tamamlama_yeni.import_destek(satir_kelimeler,"ekle")
			elif "=" in satir_kelimeler and "==" not in satir_kelimeler:
				self.kelime_tamamlama_yeni.degisken_tespit(satir_kelimeler)
		#Dosyanın değişme durumunu false yapıyoruz
		self.dosya_degistimi = False

	def renk_semalari_listele(self):
		"""Sistemde yüklü renk semalarını listeler ve döndürür"""
		#tema manager oluşturuyoruz
		tema = GtkSource.StyleSchemeManager()
		#tema ve temaların listesini döndürüyoruz
		return (tema.get_scheme_ids() ,tema)

	def renk_semasi_ata(self,sema_adi):
		"""Editöre renk şeması eklemek için yazılmış bir fonksiyon"""
		#renkleri listele fonksiyonuyla seçilebilir renkleri listeliyoruz
		tema = self.renk_semalari_listele()
		#sema_adi listede varmı bakıyoruz
		if sema_adi in tema[0]:
			#Varsa return edilen sheme managerden semayı alıyoruz
			sema = tema[1].get_scheme(sema_adi)
			#ve editörün bufferine ekliyoruz
			self.editor_buffer.set_style_scheme(sema)
		else:
			print("Şema bulunamadi : {}".format(sema_adi))

	def editor_varsayilan_ayarlar(self):
		"""Editörün Varsayılan Ayarları"""
		#Satır numaralarını gösteriyoruz
		self.editor.set_show_line_numbers(self.ebeveyn.ayarlar["Satir_no_gös"])
		self.editor.set_indent_on_tab(self.ebeveyn.ayarlar["Tab_ile_ilerlet"])
		#Otomatik girintileme olsun mu?
		self.editor.set_auto_indent(self.ebeveyn.ayarlar["Otomatik_girinti"])
		#Arkaplan falan diyor anlamadım deneyelim
		#self.editor.set_background_pattern(GtkSource.BackgroundPatternType.GRID)
		self.editor.set_highlight_current_line(self.ebeveyn.ayarlar["Aktif_satiri_vurgulu"])
		#Tab genişliği 4 olarak ayarlandı
		self.editor.set_tab_width(self.ebeveyn.ayarlar["Tab_genisligi"])
		#Tab yerine boşluk konulsun mu?
		self.editor.set_insert_spaces_instead_of_tabs(self.ebeveyn.ayarlar["Tab_yerine_bosluk"])
		#Satır sınırını göster
		self.editor.set_show_right_margin(self.ebeveyn.ayarlar["Satır_siniri_goster"])
		#Satır sınırı yeri
		self.editor.set_right_margin_position(self.ebeveyn.ayarlar["Satır_siniri"])
		#Akıllı backspace
		self.editor.set_smart_backspace(self.ebeveyn.ayarlar["Akıllı_backspace"])
		"""Home ve End tuşları için ayar"""
		if self.editor.set_smart_backspace(self.ebeveyn.ayarlar["Home_End"]) == "Kapat":
			self.editor.set_smart_home_end(GtkSource.SmartHomeEndType.DISABLED)
		if self.editor.set_smart_backspace(self.ebeveyn.ayarlar["Home_End"]) == "Önce":
			self.editor.set_smart_home_end(GtkSource.SmartHomeEndType.BEFORE)
		if self.editor.set_smart_backspace(self.ebeveyn.ayarlar["Home_End"]) == "Sonra":
			self.editor.set_smart_home_end(GtkSource.SmartHomeEndType.AFTER)
		if self.editor.set_smart_backspace(self.ebeveyn.ayarlar["Home_End"]) == "Her Zaman":
			self.editor.set_smart_home_end(GtkSource.SmartHomeEndType.ALWAYS)


		self.editor.set_show_line_marks(self.ebeveyn.ayarlar["Cizgi_isaret_gos"])




