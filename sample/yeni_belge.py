import gi, os
from gi.repository import Gtk


class YeniPencere(Gtk.Window):
	def __init__(self,ebeveyn):
		Gtk.Window.__init__(self)
		self.ebeveyn = ebeveyn

		#Başlıkla simgeyi değiştirelim
		self.set_title("Dosya Veya Dizin Oluştur")
		self.set_icon_from_file("/usr/share/icons/hicolor/scalable/apps/karpuz.svg")

		#Ana bir kutu ekleyelim ve bunu pencereye koyalım
		ana_kutu = Gtk.VBox()
		self.add(ana_kutu)
		#Üst kutumuzu ekleyelim
		ust_kutu = Gtk.HBox()
		ana_kutu.pack_start(ust_kutu, expand = False, fill = True, padding = 5)
		#Labeli ekleyelim
		olustur_lb = Gtk.Label("Oluştur")
		ust_kutu.pack_start(olustur_lb, expand = False, fill = True, padding = 5)

		#Comboyu oluşturalım
		tur_store = Gtk.ListStore(str)
		self.tur_combo = Gtk.ComboBoxText(model=tur_store)
		tur_store.append(["Dosya"])
		tur_store.append(["Dizin"])
		self.tur_combo.set_active(0)
		ust_kutu.pack_start(self.tur_combo, expand = True, fill = True, padding = 5)

		#Alttaki kutuyu oluşturalım
		alt_kutu = Gtk.HBox()
		ana_kutu.pack_start(alt_kutu, expand = False, fill = True, padding = 5)

		#Labeli oluşturup ekleyelim
		adi_lb = Gtk.Label("Adı")
		alt_kutu.pack_start(adi_lb, expand = False, fill = True, padding = 5)

		#Entryi oluşturalım
		self.adi_entry = Gtk.Entry()
		alt_kutu.pack_start(self.adi_entry, expand = True, fill = True, padding = 5)

		#Buttonla kutusuda burada
		en_alt_kutu = Gtk.HBox()
		ana_kutu.pack_start(en_alt_kutu, expand = False, fill = True, padding = 5)
		button = Gtk.Button()
		button.connect("clicked",self.olustur_tiklandi)
		button.set_label("Oluştur")
		en_alt_kutu.pack_start(button, expand = True, fill = True, padding = 5)

		#Kapatılınca bu fonksiyonu çalıştırmak için sinyali ekliyoruz
		self.connect("destroy", self.kapatiliyor)


	def olustur_tiklandi(self,widget):
		"""Pencerede oluştur düğmesine basınca bu fonksiyon çalışacak"""
		#Yazılan dosya adını alalım
		yazilan = self.adi_entry.get_text()
		#Boş değilse birşeyler yazmışsa işimize yarar
		if yazilan != "":
			#Seçili tabı alalım bunu kullanıp yolu alacağız
			secili_tab = self.ebeveyn.secili_tab_bul()
			#Tabdan yolu alalım
			yol = secili_tab.dosya_y_konum
			#Yazdığımız yolla dizini birleştirelim buraya dosya oluşacak
			olus_yol = yol + "/" +yazilan
			#Eğer bu dosya varsa arkadaşı uyaralım zaten var yani
			if os.path.exists(olus_yol):
				self.ebeveyn.mesaj_dialog_olustur("Hata","Belirtilen dizinde dosya mevcut.")
			else:
				try:
					#Yoksa dizin mi yoksa dosya mı oluşacak soralım
					secili = self.tur_combo.get_active_text()
					#Dosya ise dosya dizin ise dizin oluşturalım
					if secili == "Dosya":
						f = open(olus_yol,"w")
						f.close()
					else:
						os.makedirs(olus_yol)
					#Listeyi güncellesin nede olsa yenilik var
					self.ebeveyn.scrol_tree.dizinleri_listele(yol)
					#Pencereye teşekkür edelim ve kapatalım
					self.destroy()
				except:
					self.ebeveyn.mesaj_dialog_olustur("Hata","Dosya/Dizin Oluşturulamadı. Bir yetki sıkıntısı olabilir.")
		#Boşsa uyaralım arkadaşı
		else:
			self.ebeveyn.mesaj_dialog_olustur("Hata","Adı kısmı boş bırakılamaz")

	def kapatiliyor(self,pen):
		"""Pencerenin tekrar açılması için"""
		#Tekrar açılması için değişkeni güncelliyoruz
		self.ebeveyn.dosya_dizinci_acikmi = False







