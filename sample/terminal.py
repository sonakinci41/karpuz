import gi, os
from gi.repository import Gtk, Vte, GLib, Gio, Gdk

class Terminal(Vte.Terminal):
	def __init__(self,ebeveyn):
		Vte.Terminal.__init__(self)


		self.spawn_sync(Vte.PtyFlags.DEFAULT,
						os.path.expanduser('~'),
						["/bin/bash"],
						[],
						GLib.SpawnFlags.DO_NOT_REAP_CHILD,
						None,
						None)

		palet = [
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			]

		renkler = [
			"#f8f8f2",
			"#272822",
			"#000000",
			"#f92672",
			"#a6e22e",
			"#e6db74",
			"#66d9ef",
			"#ae81ff",
			"#a1efe4",
			"#f8f8f2",
			"#000000",
			"#c61e5b",
			"#80af24",
			"#b3aa5a",
			"#50abbc",
			"#8b67cc",
			"#7fbcb3",
			"#cbcdc8",
			]
		for sayi in range(0,16):
			oldu_mu = palet[sayi].parse(renkler[sayi])
			if not oldu_mu:
				print("Renk okunamadı :: {}".format(renkler[sayi]))

		self.set_colors(palet[0],palet[1],palet[2:])


	def komut_calistir(self,komut):
		self.spawn_sync(Vte.PtyFlags.DEFAULT,
						os.path.split(komut)[0],
						["/bin/bash"],
						[],
						GLib.SpawnFlags.DO_NOT_REAP_CHILD,
						None,
						None)
		print("Komut yürütülüyor :: {}".format(komut))
		komut = "python3 '{}'\n".format(komut)
		try:
			self.feed_child_binary(bytes(komut,"utf-8"))
		except:
			self.feed_child(komut,len(komut)+2)


	def cikti_ver(self,cikti):
		try:
			self.feed_child_binary(bytes(cikti,"utf-8"))
		except:
			self.feed_child(cikti,len(cikti)+2)





