import re,gi,os, importlib
from gi.repository import Gtk, GtkSource, GObject



class KelimeTamamlama(GObject.GObject, GtkSource.CompletionProvider):
	def __init__(self):
		GObject.GObject.__init__(self)
		#Yazılan satırın değişip değişmediğini anlamak için
		self.son_gelinen_satir = 0
		self.kutuphaneler = {}
		self.degiskenler = {}



	def do_populate(self, context):
		#Yazılan değiştiğinden dolayı onerilen kelimeleri sıfırlıyoruz
		self.onerilen_kelimeler = []
		#Cursorunun konumunu alıyoruz
		cursor_konum = context.get_iter()
		#Bazen arıza çıkarıyor bu kontrolle iteri düzeltiyoruz
		if not isinstance(cursor_konum, Gtk.TextIter):
			_, cursor_konum = context.get_iter()


		satir_numarasi = self.mevcut_satirin_numarasi(cursor_konum)
		satir_kelimeler = self.mevcut_satir_kelimeler(cursor_konum)
		son_kelime = self.son_yazilan_kelime(cursor_konum)
		son_kelime_bosluk = self.son_yazilan_kelime_bosluk(cursor_konum,satir_kelimeler)

		#Satırın değişip değişmediğini kontrol ediyoruz
		if self.son_gelinen_satir != satir_numarasi:
			#Satır değişim işlemlerini yapmak için 
			self.satir_degisti(cursor_konum)
			#Yeni satıra geçtiğimiz için değişkeni yeniliyoruz
			self.son_gelinen_satir = satir_numarasi
			

		#Satırda birşey yazmayınca yeni satırı başlamış saymıyor. Bu sebeple eğer satıda \n varsa bu satır boştur
		#dolayısıyla satırı dikkate almaya gerek yok.
		#İlk satırı alıyoruz bunun için try koyuyoruz çünkü satır boş olabiliyor
		try:
			ilk_karakter = satir_kelimeler.replace(" ","")[0]
		except:
			ilk_karakter = ""
		#Eğer ilk karakter alt satıra geç veya # ise satırla bizim işimiz yok demektir.
		if "\n" in satir_kelimeler or "#" == ilk_karakter:
			pass
		#Satırda import varsa kullanıcıya destek sunmak ve import edilen kütüphaneleri içeri aktarmayı amaçlıyoruz
		elif "import " in satir_kelimeler:
			self.import_destek(satir_kelimeler,"destek")
		elif son_kelime_bosluk and "." in son_kelime_bosluk:
			self.nokta_destek(cursor_konum,son_kelime_bosluk)

		#Onerilen kelimeleri contexte yüklüyoruz
		context.add_proposals(self, self.onerilen_kelimeler, True)

	def temizlik(self,ilk,harf):
		if harf in ilk:
			ilk = ilk.split(harf)[1]
			return ilk
		else:
			return ilk


	def nokta_destek(self,cursor_konum,son_kelime_bosluk):
		"""Fonksiyonun amacı nokta konduktan sonra kullanıcıya öneriler sunmak ilk aşamada
		veriyi noktadan önce ve sonra olarak iki aşamada almaya çalışacağız"""
		#Kelimeyi noktalardan bölüyoruz
		bolunmus = son_kelime_bosluk.split(".")
		#ilk bölünmede sıkıntı olabilir o yüzden kontroller yapıyoruz ve ilk bölümü fazlalardan temziliyoruz
		ilk = bolunmus[0]
		#bolunmesini istediğimiz yerler ve çıkartılmasını istediğimiz karakterler
		for harf in ":,;|()[]{}=":
			ilk = self.temizlik(ilk,harf)
		#ilk kütüphaneyi kütüphanelerde arıyoruz
		varmi = self.kutuphaneler.get(ilk,"Yok")
		if varmi != "Yok":
			#Varsa nokta_destek_ekleme ile ekliyoruz
			self.nokta_destek_ekleme(bolunmus,varmi)
		else:
			#Yoksada değişkenmi kontro lediyoruz
			varmi = self.degiskenler.get(ilk,"Yok")
			if varmi != "Yok":
				#Değişkenlerde varsa yine nokta_destek_ekleme ile ekliyoruz
				self.nokta_destek_ekleme(bolunmus,varmi)
			else:
				#Problem şudur ki self.deneme yazdık arıza oluyor noktalı yazılan değişkenlere destek olmalıyız
				for noktalar in bolunmus[1:]:
					#Sürekli olarak self.deneme.zıbık.ıbık gibi ekliyoruz varmı yokmu diye
					ilk = ilk+"."+noktalar
					#Aynı kontrolü yapıyoruz
					varmi = self.degiskenler.get(ilk,"Yok")
					if varmi != "Yok":
						#Değişkenlerde varsa yine nokta_destek_ekleme ile ekliyoruz
						self.nokta_destek_ekleme(bolunmus,varmi)

	def nokta_destek_ekleme(self,liste,kutuphane):
		"""Nokta ile bulduklarımızı önerilenlere ekliyoruz"""
		for madde in liste[1:]:
			if liste[-1] == madde:
				#kutuphane içinde arama yapmak için
				for kut in self.kutuphane_tespiti(kutuphane,madde):
					#Tespit edilenleri onerilenlere ekliyoruz
					self.onerilen_kelimelere_ekle(kut)
			else:
				#noktanın devamını kütüphaneye ekliyoruz
				try:
					kutuphane = getattr(kutuphane,madde)
				except:
					pass

	def degisken_ekle(self, degisken_adi, cesit):
		"""Değişkenleri listeye ekliyoruz"""
		self.degiskenler[degisken_adi] = cesit
		#print("Degisken Eklendi {},{}".format(degisken_adi, cesit))

	def degisken_tespit(self,satir_kelimeler):
		"""Kullanıcının yazdığı değişkenlerin tipini kontrol etmek amacıyla bu fonksiyonu yazıyoruz"""
		#boşluk ve tablar temizlenmeli malesef
		satir_kelimeler = satir_kelimeler.replace("\t","")
		satir_kelimeler = satir_kelimeler.replace(" ","")
		#Degiskenle atananı ayırımak için = den bölüyoruz
		bolunmus = satir_kelimeler.split("=")
		#ilk kısmı boşluklarından ayırıp degisken'e atıyoruz
		degisken = bolunmus[0].replace(" ","")
		#ikinci kısmı boşlukarından ayırıp cesit'e atıyoruz
		cesit = bolunmus[1].replace(" ","")
		#Cesit boş mu kontrol ediyoruz değilse bloğa giriyoruz
		if cesit != "\n" and cesit != "":
			#Değişken string mi değil mi kontrol ediyoruz Uzun saçma bir kontrol !!!
			if cesit[0] == '"' or cesit[0] == "'" or (len(cesit)>4 and (cesit[:5] == "str('" or\
			cesit[:5] == 'str("')):
				self.degisken_ekle(degisken,str)
			#İlk numeric mi bakıyoruz int float yada complex olabilir
			elif cesit[0].isnumeric():
				#Complex se içinde j vardur
				if "j" in cesit:
					self.degisken_ekle(degisken,complex)
				#floatsa içinde . vardır
				elif "." in cesit:
					self.degisken_ekle(degisken,float)
				#hiç biri yoksa inttir bu
				else:
					self.degisken_ekle(degisken,int)
			#int yazıp değişken tanımlanmış olabilir
			elif len(cesit)>3 and cesit[:4] == "int(":
				self.degisken_ekle(degisken,int)
			#Complex yazıp değişlen tanımlanmış olabilir
			elif len(cesit)>7 and cesit[:8] == "complex(":
				self.degisken_ekle(degisken,complex)
			#Float yazıp değişken tanımlanmış olabilir
			elif len(cesit)>5 and cesit[:6] == "float(":
				self.degisken_ekle(degisken,float)
			#Liste mi kontrol ediyoruz
			elif cesit[0] == "[" or (len(cesit)>5 and cesit[:6] == "list(("):
				self.degisken_ekle(degisken,list)
			#Tuple mı kontrol ediyoruz
			elif cesit[0] == "(" or (len(cesit)>6 and cesit[:7] == "tuple(("):
				self.degisken_ekle(degisken,tuple)
			#Range mi kontrol ediyoruz
			elif len(cesit)>5 and cesit[:6] == "range(":
				self.degisken_ekle(degisken,range)
			#Sözlük mü kontrol ediyoruz
			#Set mi kontrol ediyoruz
			elif ("," in cesit and ":" not in cesit and cesit[0] == "{") or (len(cesit)>3 and cesit[:4] == "set("):
				self.degisken_ekle(degisken,set)
			#FIX MEEEEEE kontrol eksik setle karışabilir
			elif cesit[0] == "{" or (len(cesit)>4 and cesit[:5] == "dict("):
				self.degisken_ekle(degisken,dict)
			#Frozen set mi kontrol ediyoruz
			elif (len(cesit) > 10 and cesit[:11] == "frozenset({") or (len(cesit) > 10 and\
			cesit[:11] == "frozenset(("):
				self.degisken_ekle(degisken,frozenset)
			#Bool mu kontrol ediyoruz
			elif cesit == "True" or cesit == "False" or (len(cesit)>4 and cesit[:5] == "bool("):
				self.degisken_ekle(degisken,bool)
			#Byte mı kontrol ediyoruz
			elif (len(cesit)>1 and (cesit[:2] == "b'" or cesit[:2] == 'b"')) or (len(cesit)>5 and\
			cesit[:6] == "bytes("):
				self.degisken_ekle(degisken,bytes)
			#Bytearray mi kontrol ediyoruz
			elif len(cesit)>9 and cesit[:10] == "bytearray(":
				self.degisken_ekle(degisken,bytearray)
			#Memoryview mi kontrol ediyoruz
			elif len(cesit)>10 and cesit[:11] == "memoryview(":
				self.degisken_ekle(degisken,memoryview)
			else:
				#Burada import edilmiş türlerden birimi kontrol edilecek
				#tum kutuphaneleri döngüye veriyoruz
				for kutuphane in self.kutuphaneler.keys():
					#kutuphane yazılan arkadaşa eşit mi kontrol ediyoruz
					if kutuphane == cesit or kutuphane == cesit.split(".")[0]:
						#Kutuphane str ise sanal olarak kütüphaneyi import ediyoruz
						if type(self.kutuphaneler[kutuphane]) == str:
							i = importlib.import_module(self.kutuphaneler[kutuphane])
						else:
							i = self.kutuphaneler[kutuphane]
						#Sonradan yazılanı düzenliyoruz içerikte nokta varmı yok mu bakıyoruz
						if "." in cesit:
							aranacak = cesit.split(".")[1]
						else:
							aranacak = cesit
						#parantez varsa parantezden önceki kısmı alıyoruz
						aranacak = aranacak.split("(")[0]
						#Yeni yazılanı import etmeyi deniyoruz
						try:
							i = getattr(i,aranacak)
						except:
							pass						
						self.degisken_ekle(degisken,i)

	def satir_degisti(self,cursor_konum):
		"""Satır değiştiği zaman belirli işlemler yapmamız gerekiyor örneğin import edilmişleri alacağız"""
		#Bufferi alıyoruz
		buf = cursor_konum.get_buffer()
		#eski satırın başına gelmesi için bir curosr kopyalıyoruz
		kopya_cursor_konum_ilk = cursor_konum.copy()
		#eski satırın sonuna gelmesi için bir curosr kopyalıyoruz
		kopya_cursor_konum_son = cursor_konum.copy()
		#cursoru eski satıra götürüyoruz zaten satırın başına geliyor
		kopya_cursor_konum_ilk.set_line(self.son_gelinen_satir)
		#cursoru eski satıra götürüyoruz
		kopya_cursor_konum_son.set_line(self.son_gelinen_satir)
		#cursoru satırın sonuna gönderiyoruz
		kopya_cursor_konum_son.forward_to_line_end()
		#artık satırda yazan yazıyı alıyoruz
		satir_kelimeler = buf.get_text(kopya_cursor_konum_ilk, kopya_cursor_konum_son, True)
		if "import " in satir_kelimeler:
			self.import_destek(satir_kelimeler,"ekle")
		elif "=" in satir_kelimeler and "==" not in satir_kelimeler:
			self.degisken_tespit(satir_kelimeler)

	def kutuphane_ekle(self,k_adi,kutuphane):
		"""İmport as vb. işlemler için bazen farklı isimlerde import edilebiliyor bu sebeple
		kütüphane adını ve kutuphanenin gerçek adını alıyoruz kontrol edip self.kutuphaneler e ekliyeceğiz"""
		try:
			self.kutuphaneler[k_adi] = importlib.import_module(kutuphane)
			#print("Kütüphane eklendi : {} {}".format(k_adi,kutuphane))
		except:
			pass

	def kutuphane_tespiti(self,kutuphane,kelime=False):
		"""Kutuphane altındaki fonksiyon methot vb. işlevleri tespit edip geri döndürüyoruz
		Eğer aranan bir kelime varsa kelime dikkate alınıyor yoksa dikkate almıyoruz"""
		#Donecekler için bir liste oluşturuyoruz
		donecekler = []
		#Kutuphane yoksa veya adı yanlış yazılmışsa diye bir try bloğu kuruyoruz
		try:
			#kutuphaneyi sanal olarak import ediyoruz
			if type(kutuphane) == str:
				i = importlib.import_module(kutuphane)
			else:
				i = kutuphane
			#kutuphane altında bulunanları listeliyoruz
			bulunanlar = dir(i)
			#bulunanları sıralıyoruz
			bulunanlar.sort()
			#bulunanları döngüye veriyoruz
			for kut in bulunanlar:
				#kelime verilmiş mi verilmemişmi kontrol ediyoruz
				if kelime:
					#Kelime verilmişse kutuphane içinde varmı bakıyoruz
					if kelime in kut:
						#Varsa doneceklere ekliyoruz
						donecekler.append(kut)
				else:
					#Kelime verilmemişse tümünü geri çeviriyoruz
					donecekler.append(kut)
			#Dönecekler listemizi döndürüyoruz :D müthiş açıklama
			return donecekler
		except:
			#Eğer kütüphane hatası alırsak boş liste döndürüyoruz
			return []


	def import_destek(self,satir_kelimeler,tip="destek"):
		"""Import sırasında kullanıcıya destek olmak amacıyla bu fonksiyonu yazıyoruz
		Burası bir ucubeye döndü!!!"""
		#import from ile mi yapılmış ona bakıyoruz
		#satırı boşluklarına bölüyoruz
		bolunmus_satir = satir_kelimeler.split(" ")
		if "from" in satir_kelimeler:
			#Döngüye bolunmus satırları veriyoruz 
			for sayi in range(0,len(bolunmus_satir)):
				#Gelen belge from ve arada bir boşluktan sonra import bir boşluktan sonra as varsa şablona uyuyor
				if len(bolunmus_satir) > sayi + 4 and bolunmus_satir[sayi] == "from"\
				and bolunmus_satir[sayi+2] == "import" and bolunmus_satir[sayi+4] == "as":
					#Eğer as varsa deskte şansımız yok sadece kutuphaneye ekliyoruz tabi ekle modundaysak
					if tip == "ekle":
						#kutuphaneyi düzenliyoruz
						duzenli_kutuphane = "{}.{}".format(bolunmus_satir[sayi+1],bolunmus_satir[sayi+3])
						#as ile atanan isimde düzenli kütüphaneyi ekliyoruz
						self.kutuphane_ekle(bolunmus_satir[sayi+5],duzenli_kutuphane)
				#Gelen belge from ve arada bir boşluktan sonra import varsa şablona uyuyor
				elif len(bolunmus_satir) > sayi + 2 and bolunmus_satir[sayi] == "from"\
				and bolunmus_satir[sayi+2] == "import":
					#İmporttan sonra bir kelime yazıldımı bakıyoruz
					#importtan sonrasını ayıklaması için virgül kontrolü fonksiyonuna gönderiyoruz
					virgul_ayiklanmis = self.import_virgul_kontrolu(satir_kelimeler)
					#tip ekle mi kontrol ediyorz
					if tip == "ekle":
						#virgülleri ayıklayıp gelenleri döngüye veriyoruz
						for kutuphane in virgul_ayiklanmis:
							#import sırasında * argümanı ne varsa import et anlamına geliyor bunu kontrol
							#ediyoruz
							if kutuphane == "*":
								#Verilen kütüphanenin altındaki herşeyi listeliyoruz
								for tum_kutuphaneler in self.kutuphane_tespiti(bolunmus_satir[sayi+1]):
									#gelen kutuphaneleri düzgün bir şekilde kaydetmek için formatını 
									#düzenliyoruz
									duzenli_kutuphane = "{}.{}".format(bolunmus_satir[sayi+1],tum_kutuphaneler)
									#kutuphaneleri ekleme fonksiyonuna yönlendiriyoruz
									self.kutuphane_ekle(tum_kutuphaneler,duzenli_kutuphane)
							else:
								#gelen kutuphaneleri düzgün bir şekilde kaydetmek için formatını düzenliyoruz
								duzenli_kutuphane = "{}.{}".format(bolunmus_satir[sayi+1],kutuphane)
								#kutuphaneleri ekleme fonksiyonuna yönlendiriyoruz
								self.kutuphane_ekle(kutuphane,duzenli_kutuphane)
					#Destek amaçlı fonksiyon çalıştırıldıysa
					elif tip == "destek":
						#kutuphane tespit fonksiyonuna yazılanları gönderiyoruz
						for kutuphane in self.kutuphane_tespiti(bolunmus_satir[sayi+1],virgul_ayiklanmis[-1]):
							#Tespit edilenleri onerilenlere ekliyoruz
							self.onerilen_kelimelere_ekle(kutuphane)						
		else:
			#import formu as içeriyor mu içermiyor mu bir bakıyoruz
			kontrol = True
			#satırı parçalara ayırıp döngüye veriyoruz
			for sayi in range(0,len(bolunmus_satir)):
				#Eğer import ____ as formatındaysa bloğa giriş yapılıyr
				if len(bolunmus_satir) > sayi + 2 and bolunmus_satir[sayi] == "import"\
				and bolunmus_satir[sayi+2] == "as":
					#Kontrolü False yapıyoruz alttaki if bloğu çalışmasın diye
					kontrol = False
					#Tip eklemi kontrol ediyoruz
					if tip == "ekle":
						#Kütüphane ekle fonksiyonunu ekliyoruz
						self.kutuphane_ekle(bolunmus_satir[sayi+3],bolunmus_satir[sayi+1])
			#Eğer yukarıdaki şarta uymuyorsa bu loğa giriyoruz
			if kontrol:
				for kutuphane in self.import_virgul_kontrolu(satir_kelimeler):
					#Eğer import destek ekle kipinde çalıştıysa ktüphaneyi ekliyoruz
					if tip == "ekle":
						#Kütüphane ekle fonksiyonu açlıştırıyoruz
						self.kutuphane_ekle(kutuphane,kutuphane)

	def import_virgul_kontrolu(self,satir_kelimeler):
		"""Fonksiyonun amacı çoklu import durumunda virgül yardımıyla ayıklama yapmak"""
		#satırdaki boşlukları temizliyoruz
		bolunen_importlar = satir_kelimeler.replace(" ","")
		#importtan sonraki bölümü alıyoruz
		bolunen_importlar = bolunen_importlar.split("import")[1]
		#Bİrden fazla import durumu için virgül varmı kontrol ediyoruz
		if "," in bolunen_importlar:
			#Eğer virgül varsa virgüllerden bölüyoruz ve döndürüyoruz
			return bolunen_importlar.split(",")
		else:
			#Yoksa importu direk döndürüyoruz
			return [bolunen_importlar]

	def onerilen_kelimelere_ekle(self, kelime):
		"""Çok sık ekleme işlemi olacağından önerilen kelimeleri eklemek işlemini bir fonksiyona atıyoruz"""
		self.onerilen_kelimeler.append(GtkSource.CompletionItem(label=kelime, text=kelime))

	def mevcut_satirin_numarasi(self, cursor_konum):
		"""Mevcut satırın numarasını döndürüyoruz"""
		#Methodla satır numarasını alıp döndürüyoruz
		return cursor_konum.get_line()

	def son_yazilan_kelime_bosluk(self, cursor_konum,satir_kelimeler):
		if " " not in satir_kelimeler:
			return satir_kelimeler
		#Bufferi alıyoruz
		buf = cursor_konum.get_buffer()
		#Cursor konumunun birer kopyasını alıyoruz
		kopya_cursor_bosluk = cursor_konum.copy()
		kopya_cursor_s_b = cursor_konum.copy()
		#kopya kursorunu satır başına getiriyoruz
		kopya_cursor_s_b.backward_sentence_start()
		#kopya kursorunu ilk boşluğa getirelim ve 
		ilk_bosluk = kopya_cursor_bosluk.backward_search(" ",Gtk.TextSearchFlags.VISIBLE_ONLY,
		kopya_cursor_s_b)
		#Boşluk yoksa None döndürüyor
		if ilk_bosluk != None:
			#Satırın boşluktan sonraki cursorla şuanki cursor arası olarak alıyoruz
			satir = buf.get_text(ilk_bosluk[1], cursor_konum, True)
			return satir
		else:
			return False

	def son_yazilan_kelime(self, cursor_konum):
		"""Yazılan son kelimeyi döndürüyoruz"""
		#Bufferi alıyoruz
		buf = cursor_konum.get_buffer()
		#Cursor konumunun bir kopyasını alıyoruz
		kopya_cursor_konum = cursor_konum.copy()
		#kopya kursoru ilk kelimenin önüne getiriyoruz
		kopya_cursor_konum.backward_visible_word_start()
		#bufferdan kopya cursor ile son cursor arasını alıyoruz
		son_kelime = buf.get_text(kopya_cursor_konum, cursor_konum, True)
		#son_kelime none mu kontrol ediyoruz
		if son_kelime == None:
			#None ise False dönüyor
			return False
		else:
			#Değilse son kelimeyi döndürüyoruz
			return son_kelime

	def mevcut_satir_kelimeler(self,cursor_konum):
		"""Mevcut satırdaki kelimeleri bulup geri döndürüyorum"""
		#Bufferi alıyoruz
		buf = cursor_konum.get_buffer()
		#Cursor konumunun bir kopyasını alıyoruz
		kopya_cursor_konum = cursor_konum.copy()
		#kopya kursoru ilk satır karakterinin önüne getiriyoruz
		kopya_cursor_konum.backward_sentence_start()
		#bufferdan kopya cursor ile son cursor arasını alıyoruz
		satir = buf.get_text(kopya_cursor_konum, cursor_konum, True)
		#satır none mu kontrol ediyoruz
		if satir == None:
			#None ise False dönüyor
			return False
		else:
			#Değilse son satırı döndürüyoruz
			return satir




