import gi
from gi.repository import Gtk

class Ayarlar(Gtk.Grid):
	def __init__(self,ebeveyn):
		Gtk.Grid.__init__(self)
		self.ebeveyn = ebeveyn

		self.set_row_spacing(5)
		self.set_column_spacing(5)

		####################################################
		#Temelde olay aynı labeli ekliyoruz
		satir_no_label = Gtk.Label()
		#Labele yazıyı ekliyoruz
		satir_no_label.set_text("Satır numaralarını göster")
		#Labeli satıra ekleyelim
		self.attach(satir_no_label,0,0,1,1)

		#Swichi oluşturakım
		satir_no_switch = Gtk.Switch()
		#Eğer True ise değer swich seçili gelecek
		if self.ebeveyn.ayarlar["Satir_no_gös"]:
			satir_no_switch.set_active(True)
		#Sinyalimizi fonksiyona ekliyoruz
		satir_no_switch.connect("notify::active", self.degistir, "Satir_no_gös")
		#Swichi ekliyoruz
		self.attach(satir_no_switch,1,0,1,1)
		####################################################
		tab_gotur_label = Gtk.Label()
		tab_gotur_label.set_text("Taba basıldığında seçili metni ilerlet")
		self.attach(tab_gotur_label,0,1,1,1)

		tab_gotur_switch = Gtk.Switch()
		if self.ebeveyn.ayarlar["Tab_ile_ilerlet"]:
			tab_gotur_switch.set_active(True)
		tab_gotur_switch.connect("notify::active", self.degistir, "Tab_ile_ilerlet")
		self.attach(tab_gotur_switch,1,1,1,1)
		####################################################
		tab_genisligi_label = Gtk.Label()
		tab_genisligi_label.set_text("Tab genişliği")
		self.attach(tab_genisligi_label,0,2,1,1)

		ad = Gtk.Adjustment(0, 1, 20, 1, 0, 0)
		tag_genisligi_spin = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		tag_genisligi_spin.set_hexpand(True)
		tag_genisligi_spin.connect("value-changed", self.spin_degisti, "Tab_genisligi")
		tag_genisligi_spin.set_value(self.ebeveyn.ayarlar["Tab_genisligi"])
		self.attach(tag_genisligi_spin,1,2,1,1)
		####################################################
		a_satiri_vurgula_label = Gtk.Label()
		a_satiri_vurgula_label.set_text("Aktif satırı vurgula")
		self.attach(a_satiri_vurgula_label,0,3,1,1)

		a_satiri_vurgula_switch = Gtk.Switch()
		if self.ebeveyn.ayarlar["Aktif_satiri_vurgulu"]:
			a_satiri_vurgula_switch.set_active(True)
		a_satiri_vurgula_switch.connect("notify::active", self.degistir, "Aktif_satiri_vurgulu")
		self.attach(a_satiri_vurgula_switch,1,3,1,1)
		####################################################
		otomatik_girinti_label = Gtk.Label()
		otomatik_girinti_label.set_text("Otomatik girintile")
		self.attach(otomatik_girinti_label,0,4,1,1)

		otomatik_girinti_switch = Gtk.Switch()
		if self.ebeveyn.ayarlar["Otomatik_girinti"]:
			otomatik_girinti_switch.set_active(True)
		otomatik_girinti_switch.connect("notify::active", self.degistir, "Otomatik_girinti")
		self.attach(otomatik_girinti_switch,1,4,1,1)
		####################################################
		tab_yerine_bosluk_label = Gtk.Label()
		tab_yerine_bosluk_label.set_text("Tab yerine boşluk kullan")
		self.attach(tab_yerine_bosluk_label,0,5,1,1)

		tab_yerine_bosluk_switch = Gtk.Switch()
		if self.ebeveyn.ayarlar["Tab_yerine_bosluk"]:
			tab_yerine_bosluk_switch.set_active(True)
		tab_yerine_bosluk_switch.connect("notify::active", self.degistir, "Tab_yerine_bosluk")
		self.attach(tab_yerine_bosluk_switch,1,5,1,1)
		####################################################
		metin_yaninda_cizgi_label = Gtk.Label()
		metin_yaninda_cizgi_label.set_text("Metnin yanında çizgi göster")
		self.attach(metin_yaninda_cizgi_label,0,6,1,1)

		metin_yaninda_cizgi_switch = Gtk.Switch()
		if self.ebeveyn.ayarlar["Cizgi_isaret_gos"]:
			metin_yaninda_cizgi_switch.set_active(True)
		metin_yaninda_cizgi_switch.connect("notify::active", self.degistir, "Cizgi_isaret_gos")
		self.attach(metin_yaninda_cizgi_switch,1,6,1,1)
		####################################################
		satir_siniri_goster_label = Gtk.Label()
		satir_siniri_goster_label.set_text("Satır sınırını göster")
		self.attach(satir_siniri_goster_label,0,7,1,1)

		satir_siniri_goster_switch = Gtk.Switch()
		if self.ebeveyn.ayarlar["Satır_siniri_goster"]:
			satir_siniri_goster_switch.set_active(True)
		satir_siniri_goster_switch.connect("notify::active", self.degistir, "Satır_siniri_goster")
		self.attach(satir_siniri_goster_switch,1,7,1,1)
		####################################################
		satir_siniri_konum_label = Gtk.Label()
		satir_siniri_konum_label.set_text("Satır sınırı konumu")
		self.attach(satir_siniri_konum_label,0,8,1,1)

		ad = Gtk.Adjustment(0, 10, 200, 1, 0, 0)
		tag_genisligi_spin = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		tag_genisligi_spin.set_hexpand(True)
		tag_genisligi_spin.connect("value-changed", self.spin_degisti, "Satır_siniri")
		tag_genisligi_spin.set_value(self.ebeveyn.ayarlar["Satır_siniri"])
		self.attach(tag_genisligi_spin,1,8,1,1)
		####################################################
		akilli_back_space_label = Gtk.Label()
		akilli_back_space_label.set_text("Akıllı backspace kullan")
		self.attach(akilli_back_space_label,0,9,1,1)

		akilli_back_space_switch = Gtk.Switch()
		if self.ebeveyn.ayarlar["Akıllı_backspace"]:
			akilli_back_space_switch.set_active(True)
		akilli_back_space_switch.connect("notify::active", self.degistir, "Akıllı_backspace")
		self.attach(akilli_back_space_switch,1,9,1,1)
		####################################################
		home_end_label = Gtk.Label()
		home_end_label.set_text("Home end davranışı")
		self.attach(home_end_label,0,10,1,1)

		maddeler = {"Her Zaman":0,"Önce":1,"Sonra":2,"Kapat":3}
		store = Gtk.ListStore(str)
		for madde in maddeler.keys():
			store.append([madde])

		combo = Gtk.ComboBoxText()
		combo.set_model(store)
		combo.connect("changed",self.home_end_degisti)
		combo.set_active(maddeler[self.ebeveyn.ayarlar["Home_End"]])
		self.attach(combo,1,10,1,1)
		####################################################
		arama_sonsuz_label = Gtk.Label()
		arama_sonsuz_label.set_text("Arama sonsuza kadar sürsün")
		self.attach(arama_sonsuz_label,0,11,1,1)

		arama_sonsuz_switch = Gtk.Switch()
		if self.ebeveyn.ayarlar["Arama_sonsuza_kadar_sürsün"]:
			arama_sonsuz_switch.set_active(True)
		arama_sonsuz_switch.connect("notify::active", self.degistir, "Arama_sonsuza_kadar_sürsün")
		self.attach(arama_sonsuz_switch,1,11,1,1)
		####################################################

	def degistir(self, switch, gparam, parametre):
		"""Swichler değişince bu fonksiyon çalışacak swich buraya hangi swich olduğunu
		parametre ile gönderecek button true ise true false ise false yapacağız"""
		if switch.get_active():
			self.ebeveyn.ayarlar[parametre] = True
		else:
			self.ebeveyn.ayarlar[parametre] = False
		self.ebeveyn.ayarlari_guncelle()


	def spin_degisti(self, spin_button, parametre):
		"""Spinler değişince bu fonksiyon çalışacak hangi spin olduğu parametre ile
		göndereceğiz"""
		self.ebeveyn.ayarlar[parametre] = spin_button.get_value_as_int()
		self.ebeveyn.ayarlari_guncelle()

	def home_end_degisti(self, combo):
		"""Home End değişince bu fonksiyon çalışacak"""
		#Combodan iteri alacağız
		tree_iter = combo.get_active_iter()
		#İter yani seçili olan boş değilse
		if tree_iter is not None:
			#Combodan modeli alalım
			model = combo.get_model()
			#Modelden seçili iteri alalım
			country = model[tree_iter][0]
			#Ayarlardan değiştiriyoruz
			self.ebeveyn.ayarlar["Home_End"] = country
		self.ebeveyn.ayarlari_guncelle()




