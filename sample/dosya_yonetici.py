import gi, os, subprocess
from gi.repository import Gtk, Gio

class DosyaYonetici(Gtk.ScrolledWindow):
	def __init__(self,ebeveyn):
		Gtk.Window.__init__(self)
		self.ebeveyn = ebeveyn
		#Son açılan dosya
		self.son_acilan = None

		#Scrool tipini ayarlıyoruz
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		#Dosya yöneticisindekileri gösterileceği bir liststore oluşturuyoruz themed icon ve str tutacak
		self.dosya_list_store = Gtk.ListStore(Gio.ThemedIcon(), str ,str)
		#Dosya yöneticisi olarak solda görünecek widgeti oluşturuyoruz
		self.dosya_yoneticisi = Gtk.TreeView(model=self.dosya_list_store)
		#Dosya yöneticisine tek tıklama eklemek için
		#self.dosya_yoneticisi.set_property('activate-on-single-click', True)
		#Dosya yöneticisi aktif olunca fonksiyon çalışacak
		self.dosya_yoneticisi.connect("row-activated",self.dosya_yoneticisi_tiklandi)

		#Başlık Gözükmesin istiyoruz
		self.dosya_yoneticisi.set_headers_visible(False)
		#Dosya yöneticisinin scroola ekliyoruz
		self.add(self.dosya_yoneticisi)

		#Dosya yöneticisinde görünecek dosya iconu için Pixbuf renderer oluşturuyoruz
		dy_icon = Gtk.CellRendererPixbuf()
		#Rendereri sutuna ekliyoruz stun_adı icon_renderer veri gicon
		dy_icon_sutun = Gtk.TreeViewColumn('Icon', dy_icon, gicon=0)
		#Sutunu dosya yönetisine ekliyoruz
		self.dosya_yoneticisi.append_column(dy_icon_sutun)
		#Dosya yöneticisinde görünecek dosya adı için Text renderer oluşturuyoruz
		dy_text = Gtk.CellRendererText()
		#Rendereri sutuna ekliyoruz stun_adı icon_renderer text
		dy_text_sutun = Gtk.TreeViewColumn('Dosya Adı', dy_text, text=1)
		#Sutunu dosya yönetisine ekliyoruz
		self.dosya_yoneticisi.append_column(dy_text_sutun)


	def dosya_bilgi_getir(self,yol):
		"""Listeye eklenecek olan dosyanın iconunu getirmek için bu fonksiyonu yazıyoruz"""
		#Dosyayı alıyoruz Gio.File olarak
		dosya = Gio.File.new_for_path(yol)
		#Dosyanın bilgisini alıyoruz
		bilgi = dosya.query_info("standard::*",Gio.FileQueryInfoFlags.NONE,None)
		#bilgiden iconu çekip geri döndürüyoruz
		return (bilgi.get_icon(),bilgi.get_content_type())

	def dizinleri_listele(self,yol):
		"""Dizinde verilenleri iconlarıyla düzenleyip listeye ekleyeceğiz"""
		self.son_acilan = yol
		#Dosya listesini temizliyoruz
		self.dosya_list_store.clear()
		#Dizinde bulunanları listeliyoruz
		dosyalar = os.listdir(yol)
		dosyalar.sort()
		for dosya in dosyalar:
			if not self.ebeveyn.ayarlar["Gizli_dosya_goster"] and dosya[0] == ".":
				pass
			else:
				#Urlleri oluşturuyoruz
				url = os.path.join(yol,dosya)
				#iconu istiyoruz
				icon = self.dosya_bilgi_getir(url)[0]
				#Dosya yöneticisi listemize ekliyoruz
				self.dosya_list_store.append([icon,dosya,url])
		#Yolumuz / ise yukarı çıkamayız
		if yol == "/":
			#O zaman görünümü kapatalım
			self.ebeveyn.scrol_yukari.set_sensitive(False)
		else:
			#Yoksa açalım
			self.ebeveyn.scrol_yukari.set_sensitive(True)

	def dosya_yoneticisi_tiklandi(self,widget=None,path=None,coloumn=None):
		"""Dosya yöneticisinin tıklandığında bu fonksiyon çalışacak"""
		#secileni öğrenelim
		selection = self.dosya_yoneticisi.get_selection()
		#Modelle iteri alalım
		tree_model, tree_iter = selection.get_selected()
		#Şimdilik print ediyoruz ama dosyayı açacağız nasıl yapacaksak
		yol = tree_model[tree_iter][2]
		#Yol bir dizin mi kontrol ediyoruz
		if os.path.isdir(yol):
			#Dizinse secili tabdaki dizini güncelleyelim
			secili = self.ebeveyn.secili_tab_bul()
			if secili != None:
				secili.dosya_y_konum = yol
			#Dizinimizi listeleyelim
			self.dizinleri_listele(yol)
		elif os.path.isfile(yol):
			#Dosya mime tipini alalım
			bilgi = self.dosya_bilgi_getir(yol)[1]
			#Bizim mimeler içinde varssa
			if bilgi in self.ebeveyn.mimetypes:
				#Dosyayı açtıralım
				self.ebeveyn.dosya_ac(yol)
			else:
				#Değilse açma komutunu verelim
				komut = ["xdg-open", yol]
				#Subprocess ile açalım
				subprocess.Popen(komut,shell=False)

