#!/usr/bin/python3
# -*- coding: utf-8 -*-

import gi, os, sys
from gi.repository import Gtk, Gio, Gdk, GtkSource, Pango, GObject, GLib
from sample import text_editor, dosya_yonetici, terminal, ayarlar, yeni_belge

class KarpuzMerkez(Gtk.ApplicationWindow):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		"""PENCERE TEMEL"""
		#Bizim programın açabileceği mimeler
		self.mimetypes = ["text/plain","text/css","text/javascript","text/mathml","text/x-c++hdr","text/x-c++src",
		"text/x-csrc","text/x-chdr","text/x-dtd","text/x-java","text/x-javascript","text/x-makefile","text/x-moc",
		"text/x-pascal","text/x-patch","text/x-perl","text/x-php","text/x-python","text/x-sql","text/x-tcl",
		"text/x-tex","text/xml","application/javascript","application/x-cgi","application/x-javascript",
		"application/x-perl","application/x-php","application/x-python","application/x-shellscript",
		"application/xml","application/xml-dtd","text/markdown","text/x-python3","application/x-python3"]
		#Ayarları bu sözlükte tutuyoruz
		self.ayarlar = {"Pen_boyut":(800,600),
						"Pen_konum":(0,0),
						"Tasiyici_konum":(150,450),
						"Font_adi":"Dejavu Sans Mono 11",
						"Renk_sema":"monokai",
						"Satir_no_gös":True,#Satır numaralarını gösterir
						"Tab_ile_ilerlet":True,#Seçili metini tabla değiştirmek yerine metni ilerletir
						"Tab_genisligi":4,#Tab genişliğini ayarlarız
						"Aktif_satiri_vurgulu":True,#Aktif satırı vurgulayıp vurgulamayacağımız
						"Otomatik_girinti":True,#Otomatik girintile
						"Tab_yerine_bosluk":False,#Tab yerine boşluk konsun mu
						"Cizgi_isaret_gos":True,#Metnin yanında çizgi gösterilsin mi
						"Satır_siniri_goster":False,#Satır sınırını göster
						"Satır_siniri":50,#Satır sınırı nereden çizilsin
						"Akıllı_backspace":True,#Backspaceye basıldığında öncekine kadar silecek
						"Home_End":"Her Zaman",#Önce Sonra Kapat
						"Arama_sonsuza_kadar_sürsün":False,#Arama bitince başa dönsün mü dönmesin mi
						"Kim_calisacak":"aktif",#Yada ayni
						"Gizli_dosya_goster":False,
						"Son_calisan":("",0),
						"Calisan_store_eklenenler":[],
						"Tum_yollar":""}
		#Bu minnak değişken birden fazla Yeni Dosya Dizin açılmasını engelliyor
		self.dosya_dizinci_acikmi = False

		"""UST BAR"""
		#Pencere üst bar oluşuturuyoruz
		ust_bar = Gtk.HeaderBar()
		#Ust barı pencereye ekliyoruz
		self.set_titlebar(ust_bar)
		#Ust barda kapat butonunun gözükmesi için
		ust_bar.set_show_close_button(True)

		#Bir tool bar oluşturuyoruz
		tool_bar = Gtk.Toolbar()
		#Tool barı ust bara ekliyoruz
		ust_bar.pack_start(tool_bar)

		#Arama sonuçları arasında gezmek amaçlı
		self.arama_onceki_dugme = Gtk.Button()
		#Simgeyi her zaman görünür yapıyoruz
		self.arama_onceki_dugme.set_always_show_image(True)
		#Resim alıyoruz
		resim = Gtk.Image()
		#Stocktan bir resim ekliyoruz
		resim.set_from_stock(Gtk.STOCK_GO_UP,Gtk.IconSize.BUTTON)
		#Resmi butona ekliyoruz
		self.arama_onceki_dugme.set_image(resim)
		#Birşey aramadan ilelerme yapılması olanaksız olduğundan arkadaşı inaktif yapıyoruz
		self.arama_onceki_dugme.set_sensitive(False)
		#Üstabrın sonuna arkaşadı gönderiyoruz
		ust_bar.pack_end(self.arama_onceki_dugme)

		#Arama sonuçları arasında gezmek amaçlı
		self.arama_sonraki_dugme = Gtk.Button()
		#Simgeyi her zaman görünür yapıyoruz
		self.arama_sonraki_dugme.set_always_show_image(True)
		#Resim alıyoruz
		resim = Gtk.Image()
		#Stocktan bir resim ekliyoruz
		resim.set_from_stock(Gtk.STOCK_GO_DOWN,Gtk.IconSize.BUTTON)
		#Resmi butona ekliyoruz
		self.arama_sonraki_dugme.set_image(resim)
		#Birşey aramadan ilelerme yapılması olanaksız olduğundan arkadaşı inaktif yapıyoruz
		self.arama_sonraki_dugme.set_sensitive(False)
		#Üstabrın sonuna arkaşadı gönderiyoruz
		ust_bar.pack_end(self.arama_sonraki_dugme)

		#Arama için bir entry oluşturuyoruz
		self.arama_bar = Gtk.SearchEntry(max_width_chars=25)
		#Entryi ust bara ekliyoruz
		ust_bar.pack_end(self.arama_bar)

		#Ust bara başlık ekliyoruz
		ust_bar.set_title("KARPUZ")


		"""TOOL BAR"""
		#Tool bar için Yeni Dosya toolu oluşturuyoruz
		self.tb_yeni_dosya_dugme = Gtk.ToolButton()
		#stock open simgesini veriyoruz
		self.tb_yeni_dosya_dugme.set_stock_id(Gtk.STOCK_NEW)
		#Tool bara ekliyoruz
		tool_bar.insert(self.tb_yeni_dosya_dugme, 0)
		#Üzerine gelidiğinde tool hakkında bilgi verme olayı ekliyoruz
		self.tb_yeni_dosya_dugme.set_property("has-tooltip", True)
		#Tool üzerine gelince tb_dugme_bilgisi_fonk fonksiyonu çalışacak ve fonksiyona Dosya Aç yazısı gidecek
		self.tb_yeni_dosya_dugme.connect("query-tooltip", self.tb_dugme_bilgisi_fonk,"Yeni Belge Aç")


		#Tool bar için dosya aç toolu oluşturuyoruz
		self.tb_dosya_ac_dugme = Gtk.ToolButton()
		#stock open simgesini veriyoruz
		self.tb_dosya_ac_dugme.set_stock_id(Gtk.STOCK_OPEN)
		#Tool bara ekliyoruz
		tool_bar.insert(self.tb_dosya_ac_dugme, 1)
		#Üzerine gelidiğinde tool hakkında bilgi verme olayı ekliyoruz
		self.tb_dosya_ac_dugme.set_property("has-tooltip", True)
		#Tool üzerine gelince tb_dugme_bilgisi_fonk fonksiyonu çalışacak ve fonksiyona Dosya Aç yazısı gidecek
		self.tb_dosya_ac_dugme.connect("query-tooltip", self.tb_dugme_bilgisi_fonk,"Dosya Aç")

		#Tool bar için dosya kaydet toolu oluşturuyoruz
		self.tb_dosya_kaydet_dugme = Gtk.ToolButton()
		#stock save simgesini veriyoruz
		self.tb_dosya_kaydet_dugme.set_stock_id(Gtk.STOCK_SAVE)
		#Tool bara ekliyoruz
		tool_bar.insert(self.tb_dosya_kaydet_dugme, 2)
		#Üzerine gelidiğinde tool hakkında bilgi verme olayı ekliyoruz
		self.tb_dosya_kaydet_dugme.set_property("has-tooltip", True)
		#Tool üzerine gelince tb_dugme_bilgisi_fonk fonksiyonu çalışacak ve fonksiyona Dosya Kaydet yazısı gidecek
		self.tb_dosya_kaydet_dugme.connect("query-tooltip", self.tb_dugme_bilgisi_fonk,"Dosya Kaydet")

		#Tool bar için dosya farklı kaydet toolu oluşturuyoruz
		self.tb_dosya_farkli_kaydet_dugme = Gtk.ToolButton()
		#stock save as simgesini veriyoruz
		self.tb_dosya_farkli_kaydet_dugme.set_stock_id(Gtk.STOCK_SAVE_AS)
		#Tool bara ekliyoruz
		tool_bar.insert(self.tb_dosya_farkli_kaydet_dugme, 3)
		#Üzerine gelidiğinde tool hakkında bilgi verme olayı ekliyoruz
		self.tb_dosya_farkli_kaydet_dugme.set_property("has-tooltip", True)
		#Tool üzerine gelince tb_dugme_bilgisi_fonk fonksiyonu çalışacak ve fonksiyona Dosya Farklı Kaydet yazısı 
		#gidecek
		self.tb_dosya_farkli_kaydet_dugme.connect("query-tooltip", self.tb_dugme_bilgisi_fonk,
		"Dosya Farklı Kaydet")

		#Tool bar için dosya çalıştır toolu oluşturuyoruz
		self.tb_dosya_calistir_dugme = Gtk.ToolButton()
		#stock media play simgesini veriyoruz
		self.tb_dosya_calistir_dugme.set_stock_id(Gtk.STOCK_MEDIA_PLAY)
		#Tool bara ekliyoruz
		tool_bar.insert(self.tb_dosya_calistir_dugme, 4)
		#Üzerine gelidiğinde tool hakkında bilgi verme olayı ekliyoruz
		self.tb_dosya_calistir_dugme.set_property("has-tooltip", True)
		#Tool üzerine gelince tb_dugme_bilgisi_fonk fonksiyonu çalışacak ve fonksiyona Dosya Çalıştır yazısı gidecek
		self.tb_dosya_calistir_dugme.connect("query-tooltip", self.tb_dugme_bilgisi_fonk,"Dosya Çalıştır")

		#Tool bar için font değiştir oluşturuyoruz
		self.tb_font_degis_dugme = Gtk.ToolButton()
		#stock bold simgesini veriyoruz
		self.tb_font_degis_dugme.set_stock_id(Gtk.STOCK_BOLD)
		#Tool bara ekliyoruz
		tool_bar.insert(self.tb_font_degis_dugme, 5)
		#Üzerine gelidiğinde tool hakkında bilgi verme olayı ekliyoruz
		self.tb_font_degis_dugme.set_property("has-tooltip", True)
		#Tool üzerine gelince tb_dugme_bilgisi_fonk fonksiyonu çalışacak ve fonksiyona Dosya Çalıştır yazısı gidecek
		self.tb_font_degis_dugme.connect("query-tooltip", self.tb_dugme_bilgisi_fonk,"Font Değiştir")

		#Tool bar için renk değiştir toolu oluşturuyoruz
		self.tb_renk_degis_dugme = Gtk.ToolButton()
		#stock select color simgesini veriyoruz
		self.tb_renk_degis_dugme.set_stock_id(Gtk.STOCK_SELECT_COLOR)
		#Tool bara ekliyoruz
		tool_bar.insert(self.tb_renk_degis_dugme, 6)
		#Üzerine gelidiğinde tool hakkında bilgi verme olayı ekliyoruz
		self.tb_renk_degis_dugme.set_property("has-tooltip", True)
		#Tool üzerine gelince tb_dugme_bilgisi_fonk fonksiyonu çalışacak ve fonksiyona Dosya Çalıştır yazısı gidecek
		self.tb_renk_degis_dugme.connect("query-tooltip", self.tb_dugme_bilgisi_fonk,"Renk Değiştir")

		#Tool bar için ayarlar toolu oluşturuyoruz
		self.tb_ayarlar_dugme = Gtk.ToolButton()
		#stock select color simgesini veriyoruz
		self.tb_ayarlar_dugme.set_stock_id(Gtk.STOCK_PREFERENCES)
		#Tool bara ekliyoruz
		tool_bar.insert(self.tb_ayarlar_dugme, 7)
		#Üzerine gelidiğinde tool hakkında bilgi verme olayı ekliyoruz
		self.tb_ayarlar_dugme.set_property("has-tooltip", True)
		#Tool üzerine gelince tb_dugme_bilgisi_fonk fonksiyonu çalışacak ve fonksiyona Dosya Çalıştır yazısı gidecek
		self.tb_renk_degis_dugme.connect("query-tooltip", self.tb_dugme_bilgisi_fonk,"Ayarlar")


		"""PENCERE SİMGESİ"""
		#FİX ME
		self.set_icon_from_file("/usr/share/icons/hicolor/scalable/apps/karpuz.svg")

		"""ANA PENCERE"""
		#Pencereyi iki parçaya bölmek için dikey taşyıcı ekliyoruz
		self.ana_tasiyici = Gtk.HPaned()
		#Bölümü soldan 150 pixel saga götürüyoruz
		self.ana_tasiyici.set_position(150)
		#Ana taşıyıcıyı pencereye ekliyoruz
		self.add(self.ana_tasiyici)
		#Sol ve sağ için iki ayrı taşıyıcı oluşturuyoruz
		sol_tasiyici = Gtk.VBox()
		self.sag_tasiyici = Gtk.VPaned()
		#Sağ taşıyıcıyı üstten 450 pixel aşağıda başlatıyoruz
		self.sag_tasiyici.set_position(450)
		#Sol ve sağ taşıyıcıyı ana taşıyıcıya ekliyoruz
		self.ana_tasiyici.pack1(sol_tasiyici,True)
		self.ana_tasiyici.pack2(self.sag_tasiyici,True)
		#Sağtarafıda VPanla ikiye böleceğimizden ötürü 2 kutu lazım
		sag_ust_tasiyici = Gtk.HBox()
		sag_alt_tasiyici = Gtk.HBox()
		#Kutuları sağ pana ekleyelim
		self.sag_tasiyici.pack1(sag_ust_tasiyici)
		self.sag_tasiyici.pack2(sag_alt_tasiyici)

		"""SOL TARAF"""
		butonlar = Gtk.HBox()
		sol_tasiyici.pack_start(butonlar, expand = False, fill = True, padding = 0)
		#Üst dizine çıkmak için bir icon lazım
		self.scrol_yukari = Gtk.Button()
		#Resmin sürekli gözükmesini sağlıyoruz
		self.scrol_yukari.set_always_show_image(True)
		#Resim oluşturuyoruz
		resim = Gtk.Image()
		#Stocktan bir resim ekliyoruz
		resim.set_from_stock(Gtk.STOCK_GO_UP,Gtk.IconSize.BUTTON)
		#Resmi butona ekliyoruz
		self.scrol_yukari.set_image(resim)
		#düğmeyi taşıyıcıya ekliyoruz
		butonlar.pack_start(self.scrol_yukari, expand = True, fill = True, padding = 0)

		#Dosya yöneticisinde gizli dosya göster gizle için
		self.scrol_gizli = Gtk.ToggleButton()
		#Resmi Sürekli Görünür yapalım
		self.scrol_yukari.set_always_show_image(True)
		#Resim oluşturuyoruz
		resim = Gtk.Image()
		#Stocktan bir resim ekliyoruz
		resim.set_from_stock(Gtk.STOCK_SORT_ASCENDING,Gtk.IconSize.BUTTON)
		#Resmi butona ekliyoruz
		self.scrol_gizli.set_image(resim)
		butonlar.pack_start(self.scrol_gizli, expand = True, fill = True, padding = 0)

		#Scrool alanı oluşturuyoruz
		self.scrol_tree = dosya_yonetici.DosyaYonetici(self)
		#Scroolu sol taşıyıcıya ekliyoruz
		sol_tasiyici.pack_start(self.scrol_tree, expand = True, fill = True, padding = 5)

		#Üst dizine çıkmak için bir icon lazım
		self.scrol_yeni = Gtk.Button()
		#Resmin sürekli gözükmesini sağlıyoruz
		self.scrol_yeni.set_always_show_image(True)
		#Resim oluşturuyoruz
		resim = Gtk.Image()
		#Stocktan bir resim ekliyoruz
		resim.set_from_stock(Gtk.STOCK_NEW,Gtk.IconSize.BUTTON)
		#Resmi butona ekliyoruz
		self.scrol_yeni.set_image(resim)
		#düğmeyi taşıyıcıya ekliyoruz
		butonlar.pack_start(self.scrol_yeni, expand = True, fill = True, padding = 0)



		"""SAG TARAF"""
		#Açılan her dosyanun bir tab olmasını istiyoruz bu sebeple Notebook oluşturuyoruz
		self.editor_tablar = Gtk.Notebook()
		#Tabların sonsuzlukta kaybolmasını engelleyen mantıklı bir özellik tablara scrool oluşturuyor
		self.editor_tablar.set_scrollable(True)
		#Oluşturduğumuz tabları (Notebooku) sağ taşıyıcıya ekliyoruz
		sag_ust_tasiyici.pack_start(self.editor_tablar, expand = True, fill = True, padding = 5)

		#Terminal oluşturalım
		self.terminal = terminal.Terminal(self)
		#Terminali ekleyelim
		sag_alt_tasiyici.pack_start(self.terminal, expand = True, fill = True, padding = 5)

		#Sİnyallerimizi çalıştıralım
		self.sinyaller()
		#self.connect("resizable",self.boyut_degisti)

	def sinyaller(self):
		"""Tüm Temel Widgetlerin Sinyalleri Bu Fonksiyon Altında Toplandı"""
		self.tb_dosya_ac_dugme.connect("clicked",self.d_ac_basildi)
		self.tb_yeni_dosya_dugme.connect("clicked",self.d_yeni_belge_basildi)
		self.tb_dosya_kaydet_dugme.connect("clicked",self.d_kaydet_basildi)
		self.tb_dosya_farkli_kaydet_dugme.connect("clicked",self.d_farkli_kaydet_basildi)
		self.tb_font_degis_dugme.connect("clicked",self.tb_font_degis_fonk)
		self.tb_renk_degis_dugme.connect("clicked",self.tb_renk_degis_fonk)
		self.tb_dosya_calistir_dugme.connect("clicked",self.tb_calistir)
		self.tb_ayarlar_dugme.connect("clicked",self.tb_ayarlar_basildi)
		self.arama_bar.connect("search-changed", self.arama_degisti)
		self.arama_sonraki_dugme.connect("clicked",self.arama_basildi,"sonraki")
		self.arama_onceki_dugme.connect("clicked",self.arama_basildi,"onceki")
		self.connect("key-press-event",self.tus_basildi_fonksiyon)
		self.scrol_yukari.connect("clicked",self.scrol_yukari_basildi)
		self.editor_tablar.connect("switch_page", self.aktif_tab_degisti)
		self.scrol_gizli.connect("clicked",self.scrol_gizli_basildi)
		self.scrol_yeni.connect("clicked",self.scrol_yeni_basildi)
		#Drag Drop Sinyali
		enforce_target = Gtk.TargetEntry.new('text/plain', Gtk.TargetFlags(4), 129)
		self.drag_dest_set(Gtk.DestDefaults.ALL, [enforce_target], Gdk.DragAction.COPY)
		self.connect("drag-data-received", self.birsey_birakildi)

	def birsey_birakildi(self, widget, drag_context, x,y, data,info, time):
		"""Dragtan sonra drop olunca bu fonksiyon çalışacak"""
		#Texti alalım
		text = data.get_text()
		print(data)
		text = text[7:]
		self.dosya_ac(text)

	def tb_calistir(self,widget):
		"""Tool barda çalıştır tuşuna basılınca bu fonksiyon çalışacak"""
		#Popover oluşturuyoruz
		popover = Gtk.Popover()
		#Bir taşıyıcı lazım hemen geliyor
		kutu = Gtk.VBox()

		#Dpsyalar için bir store oluşturuyoruz
		self.calistir_dosya_store = Gtk.ListStore(str)

		#ÇALIŞTIRILABİLİR BİR DOSYA AÇILMIŞSA
		if self.calistir_dosya_store_doldur():
			#Komutu yürütmek için bu düğmeyi kullanacağız
			calistir_dugme = Gtk.Button()
			#Yani çalıştır düğmeye basılırsa ne olacka şimdi
			calistir_dugme.connect("clicked",self.calistir_dugme_basildi)
			#Simgeyi her zaman görünür yapalım
			calistir_dugme.set_always_show_image(True)
			#Simgeye düğmeyi ekleylim
			calistir_dugme.set_label("Çalıştır")
			#Bir resim oluşturalım
			resim = Gtk.Image()
			#Stocktan bir resim ekliyoruz
			resim.set_from_stock(Gtk.STOCK_MEDIA_PLAY,Gtk.IconSize.BUTTON)
			#reimi düğmeye ekleyelim
			calistir_dugme.set_image(resim)
			#Düğmeyi kutuya ekleyelim
			kutu.pack_start(calistir_dugme, expand = True, fill = True, padding = 5)

			#Açık olan dosyayı çalıştır
			self.clstr_acik_d = Gtk.CheckButton()
			#Yaziyi ekleyelim
			self.clstr_acik_d.set_label("Aktif Tabı Çalıştır")
			#Düğmeyi kutuya ekleyelim
			kutu.pack_start(self.clstr_acik_d, expand = True, fill = True, padding = 5)

			#Hep ayni dosyayi çalıştır
			self.clstr_hep_a = Gtk.CheckButton()
			#Widget durumu değişirse fonksiyon çalışacak
			self.clstr_acik_d.connect("clicked", self.clstr_acik_check_bttn)
			#Widget durumu değişirse fonksiyon çalışacak
			self.clstr_hep_a.connect("clicked", self.clstr_hep_check_bttn)
			#Yaziyi ekleyelim
			self.clstr_hep_a.set_label("Hep Aynı Tabı Çalıştır")
			#Düğmeyi kutuya ekleyelim
			kutu.pack_start(self.clstr_hep_a, expand = True, fill = True, padding = 5)

			#Dosyalar comboyu oluşturalım modelide verelim
			self.clstr_dosyalar_combo = Gtk.ComboBox(model=self.calistir_dosya_store)
			self.clstr_dosyalar_combo.connect("changed",self.clstr_dosyalar_combo_basildi)

			#Aktif olan sekmeyi çalıştırın dendiyseSon_calisan
			if self.ayarlar["Kim_calisacak"] == "aktif":
				#Acik olan checkbox secili yapıyoruz
				self.clstr_acik_d.set_active(True)
				#Combo boxı kurcalanamaz yapalım
				self.clstr_dosyalar_combo.set_sensitive(False)
			#Yok ayni sekme çalıştırılacak dendiyse o zaman
			elif self.ayarlar["Kim_calisacak"] == "ayni":
				#Hep çalıştır sekmesi seçili olmalı
				self.clstr_hep_a.set_active(True)
				#Combomuzda kurcalanabilir olmalı
				self.clstr_dosyalar_combo.set_sensitive(True)
			#Bir tab kapana bilir yeni tablar açılabilir bunun için son seçilen tab hala açıkmı kontrol edelim
			sayac = 0
			eklendi_mi = True
			# Tüm tabları döngüye veriyoruz
			for calisan in self.ayarlar["Calisan_store_eklenenler"]:
				#EĞer calisan son çalışana eşitse
				if calisan == self.ayarlar["Son_calisan"]:
					#Comboda arkadaşı seçili yapıyoruz
					self.clstr_dosyalar_combo.set_active(sayac)
					eklendi_mi = False
					break
				sayac += 1
			#Eğer eklenemediyse son seçilen bir şekilde kapandıysa o zaman ilk tab aktif yapıyoruz
			if eklendi_mi:
				self.clstr_dosyalar_combo.set_active(0)
				#Yeni seçilenimizde artık bu arkadaş
				self.ayarlar["Son_calisan"] = self.ayarlar["Calisan_store_eklenenler"][0]
			#Text için bir renderer oluşturalım
			text_renderer = Gtk.CellRendererText()
			#Dosyalar comboya text rendereri ekleyelim
			self.clstr_dosyalar_combo.pack_start(text_renderer, True)
			#Text rendereri ekleyelim
			self.clstr_dosyalar_combo.add_attribute(text_renderer, "text", 0)
			#Comboyu kutuya ekleyelim
			kutu.pack_start(self.clstr_dosyalar_combo, expand = True, fill = True, padding = 5)
		else:
			#Kullanıcıya çalıştırılabilecek dosya olmadığını bildirmemiz lazım bunun için bir label
			bilgi_label = Gtk.Label()
			#Labele yazıyı ekleyelim
			bilgi_label.set_text("Çalıştırılabilir bir dosya açınız veya kaydediniz")
			#Labeli kutuya ekleyelim
			kutu.pack_start(bilgi_label, expand = True, fill = True, padding = 5)

		#kutuyuda popevere ekleyelim
		popover.add(kutu)
		#popever konumunu altında olarak ayarlayalım
		popover.set_position(Gtk.PositionType.BOTTOM)
		#İyide hangi widgetin altında
		popover.set_relative_to(widget)
		#popeverde ne varsa görünür yap
		popover.show_all()
		#popeveri popup yapp
		popover.popup()

	def pencere_boyutu_getir(self):
		gelen = self.get_size_request()
		height = self.get_data('real height')
		if not height:
			height = self.get_size()[1]
		if not width:
			width = window.get_size()[0]
		return(width,height)


	def ayarlari_guncelle(self):
		for tab in range(0,self.editor_tablar.get_n_pages()):
			#Tabı alalım
			tab = self.editor_tablar.get_nth_page(tab)
			#Ayarı güncelleyelim
			tab.editor_varsayilan_ayarlar()

	def calistir_dugme_basildi(self,widget):
		if self.ayarlar["Kim_calisacak"] == "aktif":
			secili = self.secili_tab_bul()
			secili = secili.acilan_dosya
			secili_ = secili.split()
			if secili_[0] == "Yeni" and secili_[1] == "Belge":
				self.mesaj_dialog_olustur("HATA","Yeni Belgeyi Çalıştırmamız İçin Lütfen Kaydedin")
			else:
				self.terminal.komut_calistir(secili)
		elif self.ayarlar["Kim_calisacak"] == "ayni":
			self.terminal.komut_calistir(self.ayarlar["Son_calisan"])

	def clstr_dosyalar_combo_basildi(self,widget):
		"""Çalıştırılacak dosya değişince bu fonksiyon çalışacak ve böylece biz dosyayı göndereceğiz"""
		self.ayarlar["Son_calisan"] = self.ayarlar["Calisan_store_eklenenler"][widget.get_active()]

	def clstr_acik_check_bttn(self, widget):
		"""Check button basılınca diğer Check buttonu aktif edeceğiz"""
		#Widget aktifse diğeri aktif olmamalı
		if widget.get_active():
			#Diğerinin aktifliğini kaldıralım
			self.clstr_hep_a.set_active(False)
		else:
			#Diğerinin aktif edelim
			self.clstr_hep_a.set_active(True)

	def clstr_hep_check_bttn(self, widget):
		"""Check button basılınca diğer Check buttonu aktif edeceğiz"""
		if widget.get_active():
			#Diğerinin aktifliğini kaldıralım
			self.clstr_acik_d.set_active(False)
			self.ayarlar["Kim_calisacak"] = "ayni"
			self.clstr_dosyalar_combo.set_sensitive(True)
			#Diğerinin aktif edelim
		else:
			self.clstr_acik_d.set_active(True)
			self.ayarlar["Kim_calisacak"] = "aktif"
			self.clstr_dosyalar_combo.set_sensitive(False)

	def calistir_dosya_store_doldur(self):
		"""Calisti dosya store içine açık olan ve bir yolu olan yani çalıştırılabilir olan tabları ekleyeceğiz"""
		self.calistir_dosya_store.clear()
		sayac = 0
		self.ayarlar["Calisan_store_eklenenler"] = []
		for tab in range(0,self.editor_tablar.get_n_pages()):
			tab = self.editor_tablar.get_nth_page(tab)
			dosya_adi = tab.acilan_dosya
			dosya_adi_b = dosya_adi.split()
			if not(dosya_adi_b[0] == "Yeni" and dosya_adi_b[1] == "Belge"):
				py_mimeler = ["text/x-python","application/x-python","text/x-python3","application/x-python3"]
				mime_type = self.scrol_tree.dosya_bilgi_getir(dosya_adi)
				if mime_type[1] in py_mimeler:
					self.calistir_dosya_store.append([dosya_adi])
					self.ayarlar["Calisan_store_eklenenler"].append(dosya_adi)
					sayac += 1
		if sayac == 0:
			return False
		else:
			return True

	def scrol_yeni_basildi(self,widget):
		"""Scrol gizli baslınca bu fonksiyon çalışacak"""
		#İsteğim ontane yeni belkge penceresi açamasın bu sebepten kontrol edelim
		if not self.dosya_dizinci_acikmi:
			self.dosya_dizinci_acikmi = True
			pen = yeni_belge.YeniPencere(self)
			pen.show_all()

	def scrol_gizli_basildi(self,widget):
		"""Scrol gizli baslınca bu fonksiyon çalışacak"""
		if self.scrol_gizli.get_active():
			self.ayarlar["Gizli_dosya_goster"] = True
		else:
			self.ayarlar["Gizli_dosya_goster"] = False
		secili = self.secili_tab_bul()
		if secili != None:
			#dosya konumunu alalım
			dosya_y_konum = secili.dosya_y_konum
			self.scrol_tree.dizinleri_listele(dosya_y_konum)

	def scrol_yukari_basildi(self,widget):
		"""Açık olan listede üst dizine çıkmak istiyoruz"""
		#Secili olan tabı alalım
		secili = self.secili_tab_bul()
		if secili != None:
			#dosya konumunu alalım
			dosya_y_konum = secili.dosya_y_konum
			#dosya konumunu üst dizine çıkacak hale getirelim
			dosya_y_konum = os.path.split(dosya_y_konum)[0]
			#secili taba değiştirmeden ekleyelim ki git dön yapınca olay patlamasın
			secili.dosya_y_konum = dosya_y_konum
		else:
			dosya_y_konum = self.scrol_tree.son_acilan
			#dosya konumunu üst dizine çıkacak hale getirelim
			dosya_y_konum = os.path.split(dosya_y_konum)[0]
		#artık değiştirme
		self.scrol_tree.dizinleri_listele(dosya_y_konum)


	def aktif_tab_degisti(self, widget, editor, editor_no):
		"""Aktif olan tab değişince bu fonksiyon çalışacak"""
		self.scrol_tree.dizinleri_listele(editor.dosya_y_konum)

	def tus_basildi_fonksiyon(self, widget, event):
		"""Kısayollardan bazılarını buraya ekleyeceğiz örneğin ctrl s"""
		ctrl = (event.state & Gdk.ModifierType.CONTROL_MASK)
		#Ctrl O dosya açmak için
		if ctrl and event.keyval == Gdk.KEY_o:
			self.d_ac_basildi(None)
		#Ctrl N yeni sekme aç
		elif ctrl and event.keyval == Gdk.KEY_n:
			self.d_yeni_belge_basildi(None)
		#Ctrl S dosya kaydet
		elif ctrl and event.keyval == Gdk.KEY_s:
			self.d_kaydet_basildi(None)
		#Ctrl Sift S dosya farkli kaydet
		elif ctrl and event.keyval == Gdk.KEY_S:
			self.d_farkli_kaydet_basildi(None)
		#Ctrl F arama focus
		elif ctrl and event.keyval == Gdk.KEY_f:
			self.set_focus(self.arama_bar)
		#Ctrl R çalıştır şimdilik boş
		elif ctrl and event.keyval == Gdk.KEY_r:
			pass
		#Tab değiştimi ona bakacağız ancak basılan tuş ctrl olmasın istiyoruz
		if ctrl:
			pass
		else:
			self.editor_text_degisti(True)

	def editor_text_degisti(self, ne_oldu):
		"""Editore yazı yazılınca veya kaydedince kapatınca değişip değişmediğini kontrol etmeliyiz
		Değiştiyse True Değişmediyse False"""
		#Secili tabı buluyoruz
		secili = self.secili_tab_bul()
		#Editör focus mu yani seçilimi
		if secili.editor.is_focus():
			#Tabın üst kısmını alıyoruz
			tab = self.editor_tablar.get_tab_label(secili)
			#Tab değiştimi kısmını değiştiriyoruz

	def secili_tab_bul(self):
		"""Seçili tabu geri döndürmek amaçlıyız"""
		#Aktif olan tabı öğrenmeliyiz
		secili_no = self.editor_tablar.get_current_page()
		#Aktif tabın acilan_dosya sı nedir bir dosya yolu var mı bunun için açılan dosyayı alalım
		secili_tab = self.editor_tablar.get_nth_page(secili_no)
		return secili_tab

	def arama_degisti(self,aranan):
		"""Arama barına yazılacak olan kelimeyi aramaya çalışacağız"""
		#aranan yazıyı alalım
		if type(aranan) != str:
			aranan = aranan.get_text()
		#Yazılan boşsa o zaman butonlara ihtiyaç yok
		if aranan == "":
			#Butonları kullanılmaz yapıyoruz
			self.arama_sonraki_dugme.set_sensitive(False)
			self.arama_onceki_dugme.set_sensitive(False)
			#Eski bulunan kelimelerin ışığını söndürelim
			self.arama.set_highlight(False)
		else:
			#Hangi tab seçili onu buluyoruz
			secili = self.secili_tab_bul()
			#Secili tabın bufferini alıyoruz
			buffer = secili.editor_buffer
			#Ayar için GtkSource da Settinhs ayarları çekiyoruz
			ayar = GtkSource.SearchSettings()
			#Arama sonsuza kadar devam etsin mi?
			if self.ayarlar["Arama_sonsuza_kadar_sürsün"]:
				ayar.set_wrap_around(True)
			#Ayara aranan yazıyı veriyoruz
			ayar.set_search_text(aranan)
			#Büyük küçüh harfe duyarlı olsun mu?
			ayar.set_case_sensitive(False)
			#Bir arama oluşturuyoruz ve aramaya buffer ve ayar veriyoruz
			self.arama = GtkSource.SearchContext.new(buffer,ayar)
			#Aramada bulunanların arkaplanı aydınlık olsun mu
			self.arama.set_highlight(True)
			#Burda işte underline style ekliyoruz
			self.arama.set_match_style(GtkSource.Style())
			#Aramayı ileri doğru devam ettiriyoruz
			self.arama.forward_async(buffer.get_start_iter(),None,self.arama_bitti,secili.editor, "sonraki")

	def arama_basildi(self,widget,nereye):
		"""Yukarı aşağı oklara basılınca bu fonksiyon çalışacak"""
		#Bufferi alıyoruz
		buffer = self.arama.props.buffer
		#Arama sonraki için mi yapılmış
		if nereye == "sonraki":
			#Eğer son bulunan none ise
			if self.son_bulunan_sonraki is None:
				#En sondaki iteri alıyoruz
				offset = buffer.get_end_iter()
			else:
				#None değilse son iterin bulunduğu yere mark koyuyoruz
				offset = buffer.get_iter_at_mark(self.son_bulunan_sonraki)
			#İleri doğru arama yaptırıyoruz
			self.arama.forward_async(offset, None, self.arama_bitti, self.secili_tab_bul().editor, "sonraki")
		#Arama önceki için yapılmışsa
		elif nereye == "onceki":
			#Son bulunan önceki none mi
			if self.son_bulunan_onceki is None:
				#None ise belgenin başındaki iteri alyıyoruz
				offset = buffer.get_start_iter()
			else:
				#None değilse son iteri marklıyoruz
				offset = buffer.get_iter_at_mark(self.son_bulunan_onceki)
			#Öncekine bir arama yapıyoruz
			self.arama.backward_async(offset, None, self.arama_bitti, self.secili_tab_bul().editor, "onceki")

	def arama_bitti(self, source, result, view, nereye):
		"""Arama bittiğinde bu fonksiyon çalışacak"""
		#Arama sonucunda gelenleri alıyoruz
		valid, start, end, wrapped = self.arama.forward_finish(result)
		#valid True ise yani birşey dönmüşse
		if valid:
			#Tuşları kullanılabilir yapıyoruz
			self.arama_sonraki_dugme.set_sensitive(True)
			self.arama_onceki_dugme.set_sensitive(True)
			#Bufferi getiriyoruz
			buffer = self.arama.props.buffer
			#Bufferde seçiyoruz aralığımızı
			buffer.select_range(start, end)
			#Sonraki ve önceki bulunanları marklıyoruz
			self.son_bulunan_onceki = buffer.create_mark(None, start, True)
			self.son_bulunan_sonraki = buffer.create_mark(None, end, True)
			if nereye == "sonraki":
				#Scroolu bulunana getiriyoruz
				view.scroll_mark_onscreen(self.son_bulunan_sonraki)
			elif nereye == "onceki":
				#Scroolu bulunana getiriyoruz
				view.scroll_mark_onscreen(self.son_bulunan_onceki)
		else:
			#Bulunan yoksa düğmeleri kapatıyoruz
			if nereye == "sonraki":
				self.arama_sonraki_dugme.set_sensitive(False)
			elif nereye == "onceki":
				self.arama_onceki_dugme.set_sensitive(False)

	def tb_renk_degis_fonk(self,widget):
		"""Renk değiştir düğmesine basılınca bu fonksiyon çalışacak"""
		#Popover oluşturuyoruz
		popover = Gtk.Popover()
		#Bir taşıyıcı lazım hemen geliyor
		kutu = Gtk.HBox()
		#Listedekileri tıtması için bir list store sadece string tutacak
		liste_store = Gtk.ListStore(str)
		#Bir liste tree lazım storu tutacak
		liste_tree = Gtk.TreeView(model=liste_store)
		#Listeye tek tıklama ile aktif olmayı ekliyoruz
		liste_tree.set_property('activate-on-single-click', True)
		#peki tiklaninca ne olacak çalışacak fonksiyonu yazıyoruz
		liste_tree.connect("row-activated",self.renk_degis_liste_tiklandi)
		#Tree ye texti gösterebilmesi için bir cell renderer ekliyoruz
		liste_sutun_text = Gtk.CellRendererText()
		#Listeye cell renderer sütununu ekliyoruz
		liste_sutun = Gtk.TreeViewColumn('', liste_sutun_text, text=0)
		#sutunuda treeye ekliyoruz dıdısının dıdısı
		liste_tree.append_column(liste_sutun)
		#Başlık görünmez olmalı
		liste_tree.set_headers_visible(False)
		#Varsayılan temaları ekleyeceksek önce var olan temaları öğrenmeliyiz
		tema = GtkSource.StyleSchemeManager()
		#gelsin temalar
		temalar = tema.get_scheme_ids()
		#Temaları döngüye verip stora ekleyelim
		for tema in temalar:
			liste_store.append([tema])
		#Kutuya treeyi ekleyelim
		kutu.pack_end(liste_tree,True,True,1)
		#kutuyuda popevere ekleyelim
		popover.add(kutu)
		#popever konumunu altında olarak ayarlayalım
		popover.set_position(Gtk.PositionType.BOTTOM)
		#İyide hangi widgetin altında
		popover.set_relative_to(widget)
		#popeverde ne varsa görünür yap
		popover.show_all()
		#popeveri popup yapp
		popover.popup()

	def tb_ayarlar_basildi(self,widget):
		"""Ayarlar düğmesine basılınca bu fonksiyon çalışacak"""
		#Popover oluşturuyoruz
		popover = Gtk.Popover()
		#Bir taşıyıcı lazım hemen geliyor
		kutu = ayarlar.Ayarlar(self)
		#ayarlar dosyasında pencereyi oluşturuyoruz
		popover.add(kutu)
		#popever konumunu altında olarak ayarlayalım
		popover.set_position(Gtk.PositionType.BOTTOM)
		#İyide hangi widgetin altında
		popover.set_relative_to(widget)
		#popeverde ne varsa görünür yap
		popover.show_all()
		#popeveri popup yapp
		popover.popup()

	def renk_degis_liste_tiklandi(self,widget=None,path=None,coloumn=None):
		"""Renk şeması değiştirmek için liste yada tree ne derseniz tıklanınca bu arkadaş çalışacak"""
		#listede ne seçildi öğrenelim
		selection = widget.get_selection()
		#secilenin iteri modeli nedir öğrenelim
		tree_model, tree_iter = selection.get_selected()
		#Tüm tabların rengini değiştireceğimizden tüm tabları alalaım
		for tab in range(0,self.editor_tablar.get_n_pages()):
			#Tabı alalım
			bir_tab = self.editor_tablar.get_nth_page(tab)
			#Şema değiştir fonksiyonuyla değiştirelim
			bir_tab.renk_semasi_ata(tree_model[tree_iter][0])
		#Yeni açılan tablar için ayarımızı saklayalım
		self.ayarlar["Renk_sema"] = tree_model[tree_iter][0]

	def tb_font_degis_fonk(self,widget):
		"""Font tuşuna basılınca popever oluşmasını planlıyorum kısmet"""
		#Popover oluşturuyoruz
		popover = Gtk.Popover()
		#Popovere bir kutu ekleyeceğiz onu oluşturuyoruz
		kutu = Gtk.HBox()
		#Kutumuza font choser ekleyeceğiz varsayılan bir fontchoser dialogu oluşturuyor
		fontchooser = Gtk.FontButton()
		#Varsayılan fonut ekliyoruz
		fontchooser.set_font_name(self.ayarlar["Font_adi"])
		#Font choser düğmesine basınca font ata fonksiyonu çalışacak
		fontchooser.connect("font-set",self.font_ata)
		#Kutuya ekleyelim
		kutu.pack_start(fontchooser,False,True,0)
		#Kutuyu popevere ekleyelim
		popover.add(kutu)
		#Popever Dugmenin altında çıksın diye Bottom ekliyoruz
		popover.set_position(Gtk.PositionType.BOTTOM)
		#Popeverin hangi widgetin altında çıkacağını ayarlıyoruz
		popover.set_relative_to(widget)
		#Popeverdeki widgetleri görünür yapıyoruz
		popover.show_all()
		#Popoveri gösteriyoruz
		popover.popup()

	def font_ata(self,widget):
		"""Tüm tablara font değişiminde font eklemek için bu fonksiyon yazıldı"""
		#Tüm tablara fontu atayacağız öyle tek tek olsun istemiyoruz secilen font adını alıyoruz
		self.ayarlar["Font_adi"] = widget.get_font_name()
		#Fontu Pangodan istiyoruz
		self.varsayilan_font = Pango.font_description_from_string(self.ayarlar["Font_adi"])
		for tab in range(0,self.editor_tablar.get_n_pages()):
			#Tabın font ekle fonksiyonuna fontu gönderiyoruz
			self.editor_tablar.get_nth_page(tab).font_ekle(self.varsayilan_font)

	def tb_dugme_bilgisi_fonk(self, widget, x, y, keyboard_mode, tooltip, text):
		"""Toolbardaki düğmelerin üzerine gelince bilgi amaçlı yazı çıkmasını sağlıyoruz
		Gönderilen yazıyı gönderilen widgetin tooltipine ekliyoruz"""
		tooltip.set_text(text)
		return True

	def d_farkli_kaydet_basildi(self,widget):
		dialog = Gtk.FileChooserDialog("Kaydetmek İstediğiniz Belge", self, Gtk.FileChooserAction.SAVE,
		(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
		filter_py = Gtk.FileFilter()
		#Filtremize bir isim veriyoruz
		filter_py.set_name("Python Dosyası")
		#Mime tipini belirtiyoruz
		filter_py.add_mime_type("text/x-python")
		#filtreyi ekliyoruz
		dialog.add_filter(filter_py)
		#Filtre oluşturuyoruz
		filter_text = Gtk.FileFilter()
		#Filtremize bir isim veriyoruz
		filter_text.set_name("Text Dosyası")
		#Mime tipini belirtiyoruz
		filter_text.add_mime_type("text/plain")
		#filtreyi ekliyoruz
		dialog.add_filter(filter_text)
		dialog.set_current_name("yenibelge.py")
		#Aktif tabın acilan_dosya sı nedir bir dosya yolu var mı bunun için açılan dosyayı alalım
		secili = self.secili_tab_bul()
		tab_label = self.editor_tablar.get_tab_label(secili)
		#Dialogu çalıştırıyoruz
		donen = dialog.run()
		#Eğer tamama basılmışsa
		if donen == Gtk.ResponseType.OK:
			#Dialogta verilen adresi alıyoruz
			yol = dialog.get_uri()
			#Adresi düzenliyoruz
			yol = str(yol)[7:]
			#Adreste zaten dosya varmı kontrol ediyoruz
			if os.path.exists(yol):
				#Dosya varsa kullanıcıya soruyoruz üzerine yazalımmı diye bunun için bir dialog oluşturuyoruz
				soru = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION, Gtk.ButtonsType.OK_CANCEL,
				"Dosya Mevcut")
				soru.format_secondary_text("Aşağıda belirtilen konumda dosya mevcut. Bu dosyanın üzerine yazılmasını istermisiniz?\n{}".format(yol))
				cevap = soru.run()
				#Eğer Tamama basılırsa o zaman üzerine yazıyoruz
				if cevap == Gtk.ResponseType.OK:
					#Dosyanın yolunu degistiriyoruz
					secili.acilan_dosya_degis(yol)
					#Tabın başlığını değiştiriyoruz
					tab_label.tab_baslik_ata(yol)
					#Dosyayı kaydetmeyi deniyoruz
					self.dosya_kaydet(secili)
					#Dosya yöneticisini düzeltelim
					dizin = os.path.split(yol)[0]
					self.scrol_tree.dizinleri_listele(dizin)
					secili.dosya_y_konum = dizin
				#soru dialogunu kapatıyoruz
				soru.destroy()
			else:
				#Dosyanın yolunu degistiriyoruz
				secili.acilan_dosya_degis(yol)
				#Tabın başlığını değiştiriyoruz
				tab_label.tab_baslik_ata(yol)
				#Dosyayı kaydetmeyi deniyoruz
				self.dosya_kaydet(secili)
				#Dosya yöneticisini düzeltelim
				dizin = os.path.split(yol)[0]
				self.scrol_tree.dizinleri_listele(dizin)
				secili.dosya_y_konum = dizin
		dialog.destroy()

	def d_kaydet_basildi(self,widget):
		"""Toolbarda dosya kaydet basılınca bu fonksiyon çalışacak"""
		#Aktif olan tabı öğrenmeliyiz
		secili = self.secili_tab_bul()
		baslik = secili.acilan_dosya
		#Öncelikle dosya yolu belli mi bunu kontrol ediyoruz
		baslik = baslik.split()
		#Eğer 3 parça varsa bunlar Yeni Belge ve Numeric birşeyse bunu biz oluşturmuşuzdur
		if len(baslik) == 3 and baslik[0] == "Yeni" and baslik[1] == "Belge" and baslik[2].isnumeric():
			#Bu şartlar varsa bu bir dosya yolu değildir bu yüzden burada farklı kaydet çalıştırılacak
			self.d_farkli_kaydet_basildi(None)
		else:
			#Dosya kaydet fonksiyonunu çalıştırıyoruz
			self.dosya_kaydet(secili)

	def dosya_kaydet(self,secili):
		"""Dosya kaydetmek amaçlı yazılmış bir fonksiyon"""
		#texti almamız için buffere ihtiyacımız var
		buf = secili.editor_buffer
		#dosyanın konumunu alıyoruz
		yol = secili.acilan_dosya
		#texti bufferden alalım
		text = buf.get_text(buf.get_start_iter(), buf.get_end_iter(),True)
		try:
			#Dosyayı açmaya çalışalım
			f = open(yol,"w")
			#Dosyaya yazalım
			okunan = f.write(text)
			#Dosyayı kapatalım
			f.close()
			self.terminal.cikti_ver("echo Dosya Kaydedildi : {}\n".format(yol))
			#Dosya kapanabilsin diye False yapıyoruz
			self.editor_text_degisti(False)
		except:
			#Olmazsa kullanıcıya bilgi verelim
			self.mesaj_dialog_olustur("HATA","Dosya kaydedilemedi.\n{}".format(yol))

	def d_ac_basildi(self,widget):
		"""Toolbarda dosya aç basılınca bu fonksiyon çalışacak"""
		#Dialogu oluşturuyoruz
		dialog = Gtk.FileChooserDialog("Açmak istediğiniz dosyayı seçiniz", self, Gtk.FileChooserAction.OPEN,
		(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
		#Her dosyayı açamasın diye python ve text dosyalarını seçmesi için filtre ekliyoruz
		#Filtre oluşturuyoruz
		filter_py = Gtk.FileFilter()
		#Filtremize bir isim veriyoruz
		filter_py.set_name("Python Dosyası")
		#Mime tipini belirtiyoruz
		filter_py.add_mime_type("text/x-python")
		#filtreyi ekliyoruz
		dialog.add_filter(filter_py)
		#Filtre oluşturuyoruz
		filter_py = Gtk.FileFilter()
		#Filtremize bir isim veriyoruz
		filter_py.set_name("Python Dosyası")
		#Mime tipini belirtiyoruz
		filter_py.add_mime_type("text/x-python3")
		#filtreyi ekliyoruz
		dialog.add_filter(filter_py)
		#Filtre oluşturuyoruz
		filter_text = Gtk.FileFilter()
		#Filtremize bir isim veriyoruz
		filter_text.set_name("Text Dosyası")
		#Mime tipini belirtiyoruz
		filter_text.add_mime_type("text/plain")
		#filtreyi ekliyoruz
		dialog.add_filter(filter_text)
		#Dialogu çalıştırıyoruz
		donen = dialog.run()
		#Eğer tamama basılmışsa
		if donen == Gtk.ResponseType.OK:
			self.dosya_ac(dialog.get_filename())
		dialog.destroy()

	def d_yeni_belge_basildi(self,widget):
		"""Yeni Belge Oluşturuyoruz"""
		self.editor_tab_olustur()

	def kacinci_yeni_belge_kontrol(self):
		"""Fonksiyonun mamacı açılmış olan en yüksek numaralı yeni belgeyi kontrol edip Açılacak olan belgeye
		bir numara atamak"""
		#en_buyuk_numara isimli değişkeni oluşturuyoruz
		en_buyuk_numara = 0
		#Açık olan tabları döngüye veriyoruz
		for tab in range(0,self.editor_tablar.get_n_pages()):
			#Tabun başlığını öğreniyoruz
			baslik = self.editor_tablar.get_nth_page(tab).acilan_dosya
			#Başlığı parçalıyoruz
			baslik = baslik.split()
			#Eğer 3 parça varsa bunlar Yeni Belge ve Numeric birşeyse bunu biz oluşturmuşuzdur
			if len(baslik) == 3 and baslik[0] == "Yeni" and baslik[1] == "Belge" and baslik[2].isnumeric():
				#en_buyuk_numara dan daha büyük se o zaman yeni en büyük odur
				if int(baslik[2]) > en_buyuk_numara:
					en_buyuk_numara = int(baslik[2])
		#Son olarak bulunan en büyük rakamdan 1 fazlası yani daha büyüğü olan bir dosya adı oluşturuyoruz
		return "Yeni Belge {}".format(str(en_buyuk_numara+1))

	def dosya_acikmi_kontrol(self,dosya_konum):
		"""Tablar arasında açık olanı ve tab numarasını kontrol etmek için bu fonksiyonu kullanacağız"""
		#Kontrol değişkeni
		kontrol = False
		secili = 0
		#Tabları döngüye veriyoruz
		for tab in range(0,self.editor_tablar.get_n_pages()):
			#Tabdaki açıklan dosya dosya konuma eşitse açılmıştır
			if self.editor_tablar.get_nth_page(tab).acilan_dosya == dosya_konum:
				#Kontrolü True yapıp tabın numarasını değiştiriyoruz seçili yapayacağız
				kontrol = True
				secili = tab
				#Döngü bitmeli çünkü açık olanı bulduk
				break
		#Açıksa bunu döndürüyoruz
		if kontrol:
			return (False,secili)
		return (True,secili)

	def dosya_ac(self,dosya_konum):
		"""Dosya yı açma ve temel kontrolleri burada yapacağız"""
		#Dosya varmı kontrol ediyoruz yoksa bilgi veriyoruz
		if not os.path.exists(dosya_konum):
			self.mesaj_dialog_olustur("HATA","Dosya bulunamadı\n{}".format(dosya_konum))
		#Dosya dizin mi kontrol ediyoruz dizinse bilgi veriyoruz
		elif os.path.isdir(dosya_konum):
			self.mesaj_dialog_olustur("HATA","Bu bir dizin lütfen bir dosya seçiniz\n{}".format(dosya_konum))
		else:
			#Dosyayı açmadan önce sonkez türünü kontrol edelim
			mime_type = self.scrol_tree.dosya_bilgi_getir(dosya_konum)
			mime_type = mime_type[1]
			#Eğer tür bizim açabileceğimiz türlerin içindeyse
			if mime_type in self.mimetypes:
				#Dosya açıkmı kontrol ediyoruz açık olan dosyayı tekrar açmak istemeyiz
				acikmi, secilen = self.dosya_acikmi_kontrol(dosya_konum)
				if acikmi:
					#Açık değilse açıyoruz
					self.editor_tab_olustur(dosya_konum)
				else:
					#Kullanıcıya dosyanın zaten açık olduğunu bildiriyoruz
					self.mesaj_dialog_olustur("BİLGİ", "Dosya zaten açık.\n{}".format(dosya_konum))
					#Açıksa focusu bu dosyaya getiriyoruz
					self.editor_tablar.set_current_page(secilen)
			#Değilse
			else:
				#Dosya türü desteklenmiyorrr
				self.mesaj_dialog_olustur("BİLGİ", "Dosya türü desteklenmiyor.\n{}".format(dosya_konum))

	def mesaj_dialog_olustur(self,baslik,yazi):
		"""Kullanıcıya dönüt olarak vereceğimiz basit dialoglar oluşturuyoruz"""
		dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, baslik)
		dialog.format_secondary_text(yazi)
		dialog.run()
		dialog.destroy()

	def editor_tab_olustur(self,baslik_text="Yeni Belge"):
		"""Editör İçin Yeni Bir Tab Oluşturuyoruz. Eğer Tab İçin Başlık Gelmediyse Yeni Belge Diyoruz.
		Amaç tabın üst kısmındaki başlık ve kapat düğmesini oluşturmak."""
		#Eğer yeni bir belge açılıyorsa kayıt yeri yoksa Yeni Belge geliyor belgeye Karpuza Hoş Geldiniz yazıyoruz
		if baslik_text == "Yeni Belge":
			#Her yeni belgeye Yeni Belge başlığını atamayız bu sebeple bu işi bir kontrole bağlamalıyız
			baslik_text = self.kacinci_yeni_belge_kontrol()
			okunan = ""
		#Yeni Belge değilse o zaman belge yolu vardır belge yolunu okumaua çalışıyoruz
		else:
			okunan = self.dosya_oku(baslik_text)
		#Eğer okunan varsa yani dosya okunabildiyse o zaman sıkıntı yok işlemlere devam ediyoruz
		if okunan=="" or okunan:
			#editor olusturuyoruz
			editor = text_editor.TextEditor(self,baslik_text)
			#Yazıyı ekliyoruz
			editor.belgeye_yazi_ekle(okunan)
			#Yeni bir taşıyıcı oluşturuyoruz
			tasiyici = text_editor.TextEditorTab(self,baslik_text,editor)
			#Fontu ayarlıyoruz
			editor.font_ekle(self.varsayilan_font)
			#Oluşturulanı tablara ekliyoruz
			self.editor_tablar.append_page(editor,tasiyici)
			#Tüm tabları görünür yapıyoruz
			self.editor_tablar.show_all()
			#Son eklenen tabı seçili yapıyoruz
			self.editor_tablar.set_current_page(self.editor_tablar.get_n_pages()-1)
		#Eğer okunan yoksa o zaman kullanıcıya bilgi veriyoruz okuyamadık diye
		else:
			#Dosya okunamadı diye kullanıcıya bilgi veriyoruz
			self.mesaj_dialog_olustur("HATA","Dosya okunamadı\n{}".format(baslik_text))

	def dosya_oku(self,dosya_konum):
		"""Dosyayı açmaya çalışacağız"""
		#Dosyayı açmayı deniyoruz
		try:
			#Dosyayı açmaya çalışalım
			f = open(dosya_konum,"r")
			#Dosya açıldıysa okuyalım
			okunan = f.read()
			#Dosyayı kapatalım
			f.close()
			#Okunanı geri gönderelim
			return okunan
		except:
			#Olmazsa False döndürüyoruz
			return False

###############################################################################################
###############################################################################################

class Application(Gtk.Application):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, application_id="org.example.myapp", flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE, **kwargs)
		#Bİr adın kalmalı geriye
		GLib.set_application_name("Karpuz")
		#Birde o kahreden gurbet evet kaydetmek için erken silmek içinse çok geç
		GLib.set_prgname('karpuz')

		#Pencere daha önce açıldımı bir bakalım
		self.pen = None
		self.args = []

	def do_start_up(self):
		#Açılıita otomatki çalışıyor sanırım ne olduğunu bilmiyorum
		Gtk.Application.do_startup(self)

	def do_activate(self):
		#İlk çalıştırmaadan bu fonksiyon çalışıyor ve temek işlemleri hallediyoruz
		if not self.pen:
			#Merkez pencereyi oluşturuyoruz
			self.pen = KarpuzMerkez(application=self)
			#Pencere kapanma sinyali ekliyoruz
			self.pen.connect("delete-event", self.kapatilsin_mi)
			#Kapatma işlemi için yapılacaklar
			self.pen.connect("destroy", self.ayarlari_yaz)
			#Ayarları okumaya çalışıyoruz
			self.ayarlari_oku(self.pen)
			self.pen.show_all()
		self.pen.present()

	def do_local_command_line(self, args):
		if "karpuz.py" in args:
			args.remove("karpuz.py")
		self.args = args
		return (False, args, True)


	def do_command_line(self, command_line):
		if self.pen == None:
			self.do_activate()
		for arg in self.args:
			self.pen.dosya_ac(arg)
		self.activate()
		return False

	def kapatilsin_mi(self,pen,x):
		"""Kapatmadan önce bu fonksiyon çalışacak burada tablarda değişiklik varsa öneri sunacağız
		yoksa karpuz ayarları ayar dosyasına kaydedip kapatacağız ancak burada pencere boyutları konumu vs.
		düzgün geldiğinden bunlarıda ayar dosyasında güncelliyoruz"""
		#Burda pencere boyutu doğru geliyor galiba
		pencere_boyut = pen.get_size()
		#Alalımda kaydedelim lazım oluyor
		pen.ayarlar["Pen_boyut"]=(pencere_boyut[0],pencere_boyut[1])
		#Bu eventte işler doğru nedense anlamadım ama neyse işimizi görüyor
		pencere_konum = pen.get_position()
		pen.ayarlar["Pen_konum"]=(pencere_konum[0],pencere_konum[1])
		#Taşıyıcıların konumunu alıp güncelleyelim kapanabilir yani
		pen.ayarlar["Tasiyici_konum"] = (pen.ana_tasiyici.get_position(),pen.sag_tasiyici.get_position())

		kaydedilmeyenler = []
		kaydedilmeyenler_te = []
		#Belgeler değişmiş mi kontrol edelim tabları sıralayıp her taba soralım değiştin mi?
		for tab in range(0,pen.editor_tablar.get_n_pages()):
			#Tabı alalım
			tab = pen.editor_tablar.get_nth_page(tab)
			#Tabın taşıyıcısını alalım
			tasiyici = pen.editor_tablar.get_tab_label(tab)
			buf = tab.editor_buffer
			yol = tab.acilan_dosya
			text = buf.get_text(buf.get_start_iter(), buf.get_end_iter(),True)
			d_text = pen.dosya_oku(yol)
			if text != d_text:
				#Eğer tab değiştirilmişse o zaman kaydedilmeyenlere ekleyelim
				kaydedilmeyenler.append(tab.acilan_dosya)
				kaydedilmeyenler_te.append(tab)

		if len(kaydedilmeyenler) != 0:
			#Eğer kaydedilmeyenler boş değilse yani kaydedilmeyen varsa kullanıcıya kapatmadan ne yapalım soracağız
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.NONE, "Kaydedilmemiş Dosyalar Var")
			dialog.format_secondary_text("Aşağıda belirtilen belgeler değiştirildi ancak kaydedilmedi. Ne yapmak istersiniz?\n{}".format("\n".join(kaydedilmeyenler)))
			dialog.add_buttons("Kaydet Ve Kapat", Gtk.ResponseType.APPLY)
			dialog.add_buttons("Vazgeç", Gtk.ResponseType.CANCEL)
			dialog.add_buttons("Kapat", Gtk.ResponseType.CLOSE)
			donen = dialog.run()
			#Kullanıcı kadet demişse kaydedelim
			if donen == Gtk.ResponseType.APPLY:
				for belge in kaydedilmeyenler_te:
					pen.d_kaydet_basildi(belge)
				dialog.destroy()
				return False
			#Kullanıcı vazgeçtiyse geri dönelim birşey yapmayalım
			elif donen == Gtk.ResponseType.CANCEL:
				dialog.destroy()
				return True
			#Kullanıcı kapat dediyse kapatalım
			elif donen == Gtk.ResponseType.CLOSE:
				dialog.destroy()
				return False
		else:
			return False

	def ayarlari_yaz(self, pen):
		"""Ayar dosyasını yazmak için bu fonksiyonu yazıyoruz"""
		#Dosyaya yazılacakları düzenliyoruz
		yazilacak = ""
		#Ayar dosyasını döngüye verelim
		for ayar in pen.ayarlar.keys():
			#keysi verip değeri alalım
			deger = pen.ayarlar[ayar]
			#Eğer Calisan store eklenenler yada son calisansa işimiz yok pass
			if ayar == "Calisan_store_eklenenler" or ayar == "Son_calisan" or ayar == "Tum_yollar":
				pass
			#Pencere boyutuysa veya konumsa ona göre düzenliyoruz
			elif ayar == "Pen_boyut" or ayar == "Pen_konum" or ayar == "Tasiyici_konum":
				yazilacak += ayar
				yazilacak += ":{}|{}\n".format(str(deger[0]),str(deger[1]))
			#Değilse str çevirip yazıyoruz
			else:
				yazilacak += ayar
				yazilacak += ":{}\n".format(deger)
		tum_yollar = ""
		#Belgeler değişmiş mi kontrol edelim tabları sıralayıp her taba soralım değiştin mi?
		for tab in range(0,pen.editor_tablar.get_n_pages()):
			#Tabı alalım
			tab = pen.editor_tablar.get_nth_page(tab)
			#Tüm yollar
			tum_yollar += tab.acilan_dosya + "|"
		#Bulduklarımızı tüm yollar adı altında Yazilacaklara ekleyelim
		yazilacak += "Tum_yollar:{}\n".format(tum_yollar[:-1])
		try:
			#Kullanıcı dizinini alalım
			yol = os.path.expanduser("~")
			#dizini tamamlayalım
			yol += "/.config/karpuz"
			#Yol varmı kontrol edelim yoksa oluşturalım
			if not os.path.exists(yol):
				os.mkdir(yol)
			#dizine dosyayı ekleyelim
			yol += "/karpuz.config"
			#Dosyayı açıp yazalım kapatalım
			f = open(yol, "w")
			f.write(yazilacak)
			f.close()
		except:
			#Yazılamazsa kullanıcıya bilgi verelim
			pen.mesaj_dialog_olustur("Hata","Ayar dosyası yazılamadı.")
		Gtk.main_quit()

	def ayarlari_oku(self, pen):
		"""Açılışta ayar dosyasını okumaya çalışacağız"""
		#Ayar dosyasının konumu
		dosya = "/home/trlinux/.config/karpuz/karpuz.config"
		#Açılacak dosyyalar boş dursun bir
		acilacaklar = []
		try:
		#Dosya varsa okuyacağız sonuçta
			if os.path.exists(dosya):
				#Dosayayı açıp okuyalım
				f = open(dosya,"r")
				okunan = f.read()
				f.close()
				#Satırları \n den parçalayalım döngüye verelim
				okunan = okunan.split("\n")
				acilacaklar = []
				for satir in okunan:
					#":" ile ayırdık o yüzden birde burdan bölelim
					bol = satir.split(":")
					#Bölme sonucu iki parça olmalı tek se satır sonudur bizi ilgilendirmez
					if len(bol) == 2:
						#Pencere boyutu tuple olduğundan ona göre düzenleyip alalım
						if bol[0] == "Pen_boyut" or bol[0] == "Pen_konum" or bol[0] == "Tasiyici_konum":
							deger = bol[1]
							deger = deger.split("|")
							pen.ayarlar[bol[0]] = (int(deger[0]),int(deger[1]))
						elif bol[0] == "Tum_yollar":
							#Yollar arasına | koyuyoruz
							acilacaklar = bol[1].split("|")
							#ama boşsa "" olur oda işimiz gelmez değiştirelim
							if acilacaklar == [""]:
								acilacaklar = []
						#True yada False ise boola çevirelim
						elif bol[1] == "True":
							pen.ayarlar[bol[0]] = True
						elif bol[1] == "False":
							pen.ayarlar[bol[0]] = False
						#isdecimal mı yani rakamlarda oluştuğu için
						elif bol[1].isdecimal():
							pen.ayarlar[bol[0]] = int(bol[1])
						#O zaman stringtir direk strye çevirip basalım gitsin
						else:
							pen.ayarlar[bol[0]] = str(bol[1])
		except:
			pen.mesaj_dialog_olustur("Hata","Ayar dosyası okunamadı.")
		#Ayarları yazalım yada yazamayalım ama uygulayalım
		#Pencere boyutu ayarlayalım
		pen.resize(pen.ayarlar["Pen_boyut"][0],pen.ayarlar["Pen_boyut"][1])
		pen.move(pen.ayarlar["Pen_konum"][0],pen.ayarlar["Pen_konum"][1])
		pen.ana_tasiyici.set_position(pen.ayarlar["Tasiyici_konum"][0])
		pen.sag_tasiyici.set_position(pen.ayarlar["Tasiyici_konum"][1])
		#Fontu alıyoruz
		pen.varsayilan_font = Pango.font_description_from_string(pen.ayarlar["Font_adi"])
		#Açılacak dosyaları açılacaklara atmıştık eğer açılacak dosyalar boş değilse
		if len(acilacaklar) != 0:
			#Açılacakları döngüye veriyoruz
			for acilacak in acilacaklar:
				ayir = acilacak.split(" ")
				#Yeni belgeyse o zaman numarası bizi ilgilendirmez yeni belge açacağız
				if ayir[0] == "Yeni" and ayir[1] == "Belge":
					pen.editor_tab_olustur()
				#Değilse dosya yolu vardır onu açacağız
				else:
					pen.dosya_ac(acilacak)
		#Eğer boşsa bir yeni belge bizden hediye
		else:
			pen.editor_tab_olustur()

if __name__ == '__main__':
	app = Application()
	app.run(sys.argv)

