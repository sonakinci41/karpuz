# Karpuz

Python3 is a text editor specially developed for python3 programming using Gtk

## Dependencies Debian Packages

> python3-gi

> python3-gi-cairo

> python3-setuptools

> gir1.2-gtk-3.0

> gir1.2-gtksource-3.0

> gir1.2-vte-2.91


## How to install

Read for requirements.txt and login root user

> python3 setup.py install

![alt text](https://i.ibb.co/mhHNb8y/karpuz.png)
